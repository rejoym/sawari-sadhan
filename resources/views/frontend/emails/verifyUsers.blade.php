<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>
<body>
<h1>Welcome,{{ $user[0]->username}}</h1>
<br/>
<div>
  <p>Thank you for creating an account with the Sawari Sadhan. Please follow the link below to verify your email address</p>
</div>
<a href="{{url('user/verify/' .$user[0]->token)}}">Verify Email</a>
<div>
  <p>If you didn't register in Sawari Sadhan, please ignore this mail</p>
</div>
</body>

</html>