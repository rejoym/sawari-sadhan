@extends('frontend._shared.layouts.master')
@section('main-content')
<?php
  $gender = [
    'male' => 'Male',
    'female' => 'Female',
    'others' => 'Others'
  ];
  ?>
<div class="b-breadCumbs s-shadow">
      <div class="container" data-wow-delay="0.5s">
        <a href="home.html" class="b-breadCumbs__page">Home</a><span class="fa fa-angle-right"></span><a href="submit1.html" class="b-breadCumbs__page m-active">Register</a>
      </div>
    </div>
</div>
<div class="b-submit">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
            <aside class="b-submit__aside">
              <div class="b-submit__aside-step m-active" data-wow-delay="0.5s">
                <h3>Step 1</h3>
                <div class="b-submit__aside-step-inner m-active clearfix">
                  <div class="b-submit__aside-step-inner-icon">
                    <span class="fa fa-car"></span>
                  </div>
                  <div class="b-submit__aside-step-inner-info">
                    <h4>Register</h4>
                    <p>Register User</p>
                    <div class="b-submit__aside-step-inner-info-triangle"></div>
                  </div>
                </div>
              </div>
              <div class="b-submit__aside-step" data-wow-delay="0.5s">
                <h3>Step 2</h3>
                <div class="b-submit__aside-step-inner clearfix">
                  <div class="b-submit__aside-step-inner-icon">
                    <span class="fa fa-list-ul"></span>
                  </div>
                  <div class="b-submit__aside-step-inner-info">
                    <h4>Verify Email</h4>
                    <p>Verify your registration</p>
                  </div>
                </div>
              </div>
              <div class="b-submit__aside-step" data-wow-delay="0.5s">
                <h3>Step 3</h3>
                <div class="b-submit__aside-step-inner clearfix">
                  <div class="b-submit__aside-step-inner-icon">
                    <span class="fa fa-photo"></span>
                  </div>
                  <div class="b-submit__aside-step-inner-info">
                    <h4>Login</h4>
                    <p>Login to Sawari Sadhan</p>
                  </div>
                </div>
              </div>
              <div class="b-submit__aside-step" data-wow-delay="0.5s">
                <h3>Step 4</h3>
                <div class="b-submit__aside-step-inner clearfix">
                  <div class="b-submit__aside-step-inner-icon">
                    <span class="fa fa-user"></span>
                  </div>
                  <div class="b-submit__aside-step-inner-info">
                    <h4>View Vehicles</h4>
                    <p>View range of vehicles</p>
                  </div>
                </div>
              </div>
              <div class="b-submit__aside-step" data-wow-delay="0.5s">
                <h3>Step 5</h3>
                <div class="b-submit__aside-step-inner clearfix">
                  <div class="b-submit__aside-step-inner-icon">
                    <span class="fa fa-globe"></span>
                  </div>
                  <div class="b-submit__aside-step-inner-info">
                    <h4>BUY &amp; SELL</h4>
                    <p>Sawari of your choice</p>
                  </div>
                </div>
              </div>
            </aside>
          </div>
          <div class="col-lg-9 col-md-8 col-sm-7 col-xs-6">
            <div class="b-submit__main">
              <header class="s-headerSubmit s-lineDownLeft" data-wow-delay="0.5s">
                <h2 class="">Sign Up</h2>
              </header>
              <form class="s-submit clearfix" action="" method="POST">
                {!! csrf_field() !!}
                <div class="row">
                  <div class="col-md-6 col-xs-12">
                    <div class="b-submit__main-element{{$errors->has('input-fname') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="first-name">First Name <span>*</span></label>
                      <div class='s-relative'>
                        <input type="text" name="input-fname" id="first-name" placeholder="First Name">
                        <span>{{$errors->first('input-fname')}}</span>
                      </div>
                    </div>

                    <div class="b-submit__main-element{{$errors->has('input-lname') ? 'has-error' : '' }}" data-wow-delay="0.5s">
                      <label for="last-name">Last Name <span>*</span></label>
                      <div class='s-relative'>
                        <input type="text" name="input-lname" id="last-name" placeholder="Last Name">
                        <span>{{$errors->first('input-lname')}}</span>
                      </div>
                    </div>

                    <div class="b-submit__main-element{{$errors->has('input-email') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="email">Email <span>*</span></label>
                      <div class='s-relative'>
                        <input type="text" name="input-email" id="email" placeholder="you@example.com">
                        <span>{{$errors->first('input-email')}}</span>
                      </div>
                    </div>

                    <!-- <div class="b-submit__main-element{{$errors->has('re-password') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="re-password">Re-confirm Password <span>*</span></label>
                      <div class='s-relative'>
                        <input type="password" name="re-password" id="re-password" placeholder="***********">
                        <span>{{$errors->first('re-password')}}</span>
                      </div>
                    </div> -->
                    <div class="b-submit__main-element{{$errors->has('input-city') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="city">City <span>*</span></label>
                      <div class='s-relative'>
                        <input type="text" name="input-city" id="city" placeholder="City">
                        <span>{{$errors->first('input-city')}}</span>
                      </div>
                    </div>
                    <div class="b-submit__main-element{{$errors->has('input-phone') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="phone">Phone Number <span>*</span></label>
                      <input placeholder="Phone Number" type="text" name="input-phone" id="phone" />
                      <span>{{$errors->first('input-phone')}}</span>
                    </div>
                    <div class="b-submit__main-element{{$errors->has('input-address') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="address">Address <span>*</span></label>
                      <input placeholder="Address" type="text" name="input-address" />
                      <span>{{$errors->first('input-address')}}</span>
                    </div>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <div class="b-submit__main-element" data-wow-delay="0.5s">
                      <label for="middle-name">Middle Name</label>
                      <div class='s-relative'>
                        <input type="text" name="input-mname" id="middle-name" placeholder="Middle Name">
                      </div>
                    </div>
                    <div class="b-submit__main-element{{$errors->has('input-username') ? : ''}}" data-wow-delay="0.5s">
                      <label for="username">Username <span>*</span></label>
                      <div class='s-relative'>
                        <input type="text" name="input-username" id="username" placeholder="Username">
                        <span>{{$errors->first('input-username')}}</span>
                      </div>
                    </div>
                    <div class="b-submit__main-element{{$errors->has('input-password') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="password">Password <span>*</span></label>
                      <div class='s-relative'>
                        <input type="password" name="input-password" id="password" placeholder="**********">
                        <span>{{$errors->first('input-password')}}</span>
                      </div>
                    </div>
                    <div class="b-submit__main-element{{$errors->has('input-gender') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="gender">Gender <span>*</span></label>
                      <div class='s-relative'>
                        <select class="m-select" name="input-gender" id="gender">
                          <option value="">Select Gender</option>
                          @foreach($gender as $key=>$value)
                            <option value="{{$key}}">{{$value}}</option>
                          @endforeach
                        </select>
                        <span class="fa fa-caret-down"></span>
                        <span>{{$errors->first('input-gender')}}</span>
                      </div>
                    </div>

                    <div class="b-submit__main-element{{$errors->has('input-dob') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="dob">Date Of Birth<span>*</span></label>
                      <div class='s-relative'>
                        <input type="date" name="input-dob" id="dob">
                        <span>{{$errors->first('input-dob')}}</span>
                      </div>
                    </div>
                    <div class="b-submit__main-element{{$errors->has('input-country') ? 'has-error' : ''}}" data-wow-delay="0.5s">
                      <label for="country">Country <span>*</span></label>
                      <div class='s-relative'>
                        <input type="text" name="input-country" id="country" placeholder="Country">
                        <span>{{$errors->first('input-country')}}</span>
                      </div>
                    </div>

                  </div>
                </div>
                <button type="submit" class="btn m-btn pull-right" data-wow-delay="0.5s">Register &amp; PROCEED to next step<span class="fa fa-angle-right"></span></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>


@stop