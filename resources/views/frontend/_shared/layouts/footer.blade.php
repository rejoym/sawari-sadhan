    <footer class="b-footer">
      <a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>
      <div class="container">
        <div class="row">
          <div class="col-xs-4">
            <div class="b-footer__company wow">
              <div class="b-nav__logo">
                <h3><a href="#">Sawari <span>Sadhan</span></a></h3>
              </div>
              <p>&copy; 2019 Sawari Sadhan</p>
            </div>
          </div>
          <div class="col-xs-8">
            <div class="b-footer__content wow">
              <div class="b-footer__content-social">
                <a href="#"><span class="fa fa-facebook-square"></span></a>
                <a href="#"><span class="fa fa-twitter-square"></span></a>
                <a href="#"><span class="fa fa-instagram"></span></a>
              </div>
              <nav class="b-footer__content-nav">
                <ul>
                  <li><a href="#">Home</a></li>
                  <li><a href="#">About</a></li>
                  <li><a href="#">Buy</a></li>
                  <li><a href="#">Sell</a></li>
                  <li><a href="#">Blog</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </footer><!--b-footer-->
    <!--Main-->
    <script src="{{ asset('public/assets/frontend/js/jquery-1.11.3.min.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/js/modernizr.custom.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/js/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/js/classie.js') }}"></script>
    <!--Switcher-->
    <!-- <script src="{{ asset('assets/plugins/switcher/js/switcher.js') }}"></script> -->
    <!--Owl Carousel-->
    <script src="{{ asset('public/assets/frontend/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
    <!--bxSlider-->
    <script src="{{ asset('public/assets/frontend/plugins/bxslider/jquery.bxslider.js') }}"></script>
    <!-- jQuery UI Slider -->
    <script src="{{ asset('public/assets/frontend/plugins/slider/jquery.ui-slider.js') }}"></script>
    <!--Theme-->
    <script src="{{ asset('public/assets/frontend/js/jquery.smooth-scroll.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/js/wow.min.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/js/jquery.placeholder.min.js') }}"></script>
    <script src="{{ asset('public/assets/frontend/js/theme.js') }}"></script>
  </body>
</html>