<!DOCTYPE html>
<html>

<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Sawari Sadhan</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/assets/frontend/images/favicon.png') }}" />
    <link href="{{ asset('public/assets/frontend/css/master.css') }}" rel="stylesheet">
    <link href="{{ asset('public/assets/frontend/css/custom.css') }}" rel="stylesheet">
    <!-- SWITCHER -->
    <!-- <link rel="stylesheet" id="switcher-css" type="text/css" href="{{ asset('assets/plugins/switcher/css/switcher.css') }}" media="all" /> -->
    <!-- <link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color1.css" title="color1" media="all" data-default-color="true" /> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/frontend/plugins/switcher/css/color2.css') }}" title="color2" media="all" />
    <!-- <link rel="alternate stylesheet" type="text/css" href="{{ asset('assets/plugins/switcher/css/color3.css') }}" title="color3" media="all" /> -->
    <!-- <link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color4.css" title="color4" media="all" /> -->
    <!-- <link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color5.css" title="color5" media="all" /> -->
    <!-- <link rel="alternate stylesheet" type="text/css" href="assets/switcher/css/color6.css" title="color6" media="all" /> -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="m-index" data-scrolling-animations="true" data-equal-height=".b-auto__main-item">
    <!-- Loader -->
    <div id="page-preloader"><span class="spinner"></span></div>
    <!-- Loader end -->
    <header class="b-topBar wow">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-xs-6">
            <div class="b-topBar__tel">
              <span class="fa fa-phone"></span>
              Call Us 9813698336
            </div>
          </div>
          <div class="col-md-2 col-xs-6">

          </div>
          <div class="col-md-4 col-xs-6">
            <nav class="b-topBar__nav">
              <ul>
                <li><a href="{{ url('customer/signup') }}">Register</a></li>
                <li><a href="{{ url('customer/login') }}">Sign in</a></li>
              </ul>
            </nav>
          </div>
          <div class="col-md-2 col-xs-6">
            <div class="b-topBar__lang">
              <div class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle='dropdown'>4 Wheeler</a>
                {{-- <a class="m-langLink dropdown-toggle" data-toggle='dropdown' href="#"><span class="b-topBar__lang-flag m-en"></span>4W<span class="fa fa-caret-down"></span></a> --}}
                <a class="m-langLink dropdown-toggle" data-toggle='dropdown' href="#"><i class="fa fa-car"></i> &nbsp;4W<span class="fa fa-caret-down"></span></a>
                <ul class="dropdown-menu h-lang">
                  <li><a class="m-langLink dropdown-toggle" href="#"><i class="fa fa-car"></i> &nbsp;4W</a></li>
                  <li><a class="m-langLink dropdown-toggle"  href="#"><i class="fa fa-motorcycle"></i> &nbsp;2W</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header><!--b-topBar-->

    <nav class="b-nav">
      <div class="container">
        <div class="row">
          <div class="col-sm-3 col-xs-4">
            <div class="b-nav__logo wow">
              <h3><a href="#">Sawari <span>Sadhan</span></a></h3>
              <h2><a href="#">AUTO COLLECTION</a></h2>
            </div>
          </div>
          <div class="col-sm-9 col-xs-8">
            <div class="b-nav__list wow">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <div class="collapse navbar-collapse navbar-main-slide" id="nav">
                <ul class="navbar-nav-menu">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Buy</a></li>
                  <li><a href="#">Sell</a></li>
                  <li><a href="#">Compare</a></li>
                  <li><a href="#">Blog</a></li>
                  <li><a href="c#">Contact</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav><!--b-nav-->