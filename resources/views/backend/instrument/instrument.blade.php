<div class="heading">
                      <i class="fa fa-gear"></i>Instruments
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="warning-indicator">Warning Indicator</label>
                          <select class="form-control" multiple="" id="warning-indicator" name="input-warning-indicator[]">
                            @foreach($data['all_warning_type'] as $warning_types)
                              <option value="{{$warning_types->id}}">{{ucfirst($warning_types->warning_indicator_type)}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="adv-supervision-cluster">Advanced supervision cluster</label>
                          <select class="form-control" multiple="" id="adv-supervision-cluster" name="input-adv-cluster[]">
                            @foreach($data['all_advanced_supervision'] as $adv_supervision)
                              <option value="{{$adv_supervision->id}}">{{ucfirst($adv_supervision->advanced_supervision_cluster)}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="multi-info-display-device">Multi Information Display Device</label>
                          <select class="form-control" multiple="" id="multi-info-display-device" name="input-multi-info[]">
                            @foreach($data['all_multi_info_device'] as $all_device)
                              <option value="{{$all_device->id}}">{{ucfirst($all_device->multi_information_display_device)}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div><!--product div ends-->