@extends('backend._shared.layouts.master')
@section('main-content')
  <div class="row">
    <div class="col-md-12">
      <div class="widget-container my-container">
        <div class="heading">
          <i class="fa fa-shield"></i>Add New Model
        </div>
        <div class="my-widget-content">
          <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil impedit molestiae illo. Aspernatur quasi, sed commodi, a aliquam distinctio voluptate!</span>
        </div>
        <div class="row widget-alert-wrapper">
          <div class="widget-alert col-md-10 model-widget-alert">
            @if(Session::get('Success'))
              <div class="alert alert-success my-widget-alert" role="alert">
                <i class="fa fa-check"></i> Success! New Model has been successfully registered.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif
            @if($errors->has('input-model-name') || $errors->has('input-brand'))
              <div class="alert alert-danger my-widget-alert" role="alert">
                <i class="fa fa-exclamation-triangle"></i> Error! Form Validation Error! Please try again.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif
          </div>
        </div>
        <div class="widget-content padded">
          <form action="" id="validate-form" method="post">
            {!! csrf_field() !!}
            <fieldset>
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="modelname">Model Name *</label>
                    <input class="form-control" id="modelname" name="input-model-name" type="text" placeholder="Enter Model Name" value="{{old('input-model-name')}}"/>
                    <span class="my-form-error">{{$errors->first('input-model-name')}}</span>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label">Brand Name *</label>
                    <select class="form-control" name="input-brand">
                      <option value="">Select Brand</option>
                      @foreach($data['all_brand'] as $brands)
                        <option value="{{$brands->id}}" {{(old('input-brand')==$brands->id) ? 'selected' : '' }}>{{ucfirst($brands->brand_name)}}</option>
                      @endforeach
                    </select>
                    <span class="my-form-error">{{$errors->first('input-brand')}}</span>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <!-- Striped Table -->
    <div class="col-lg-12">
      <div class="widget-container fluid-height clearfix my-container">
        <div class="heading">
          <i class="fa fa-table"></i>All Available Models
        </div>
        <div class="widget-content padded clearfix">
          <table class="table table-striped">
            <thead>
              <th>S-N</th>
              <th>Brand</th>
              <th>Model</th>
              <th>Action</th>
            </thead>
            <tbody>
              <?php $sn = 0; ?>
              @foreach($data['all_model_brand'] as $models)
              <tr>
                <td>
                  {{++$sn}}
                </td>
                <td>
                  {{ucfirst($models->brands->brand_name)}}
                </td>
                <td>
                  {{ucfirst($models->modal_name)}}
                </td>
                <td>
                  <a href="#" class="record-edit"><i class="fa fa-pencil"></i> Edit</a> &nbsp;|&nbsp; <a href="#" class="record-delete"><i class="fa fa-trash-o"></i> Delete</a>
                </td>
                @endforeach
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@stop