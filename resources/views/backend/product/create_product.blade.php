@extends('backend._shared.layouts.master')
@section('main-content')
<!-- <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#product">Product</a></li>
  <li><a data-toggle="tab" href="#safety">Safety</a></li>
  <li><a data-toggle="tab" href="#misc">Miscellaneous</a></li>
</ul> -->
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="row widget-alert-wrapper product-alerts">
        <div class="widget-alert col-md-12 variant-widget-alert">
          @if(Session::get('Success'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! New Product has been successfully registered.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @if($errors->has('input-product-name') || $errors->has('input-brand') || $errors->has('input-model') || $errors->has('input-variant') || $errors->has('input-price'))
          <div class="alert alert-danger my-widget-alert" role="alert">
            <i class="fa fa-exclamation-triangle"></i> Error! Form Validation Error! Please try again.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div>
      </div>
      <div class="widget-content padded">
        <form action="" id="validate-form" method="post" class="product-form">
          {!! csrf_field() !!}
          <fieldset>
            <div class="tab-content">
              <div id="product" class="tab-pane fade in active">
                <div class="heading no-padding-lr">
                  <i class="fa fa-car"></i>Add New Product
                </div>
                <div class="heading no-padding-lr gray">
                  <i class="fa fa-th-list"></i>Select Specifications
                </div>
                <div class="heading-info">
                  <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="brand" class="control-label">Select Brand</label>
                      <select class="form-control" name="input-brand" id="brand-product">
                        <option value="">Brand</option>
                        @foreach($data['all_brand'] as $brands)
                        <option value="{{$brands->id}}">{{ucfirst($brands->brand_name)}}</option>
                        @endforeach
                      </select>
                      <span class="my-form-error">{{$errors->first('input-brand')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="model" class="control-label">Model</label>
                      <select class="form-control" name="input-model" id="model-product">
                        <option value="">Please Select a Brand First</option>
                      </select>
                      <span class="my-form-error">{{$errors->first('input-model')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="variant" class="control-label">Variant</label>
                      <select class="form-control" name="input-variant" id="variant-product">
                        <option value="">Please Select a Model First</option>
                      </select>
                      <span class="my-form-error">{{$errors->first('input-variant')}}</span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="price">Price</label>
                      <input class="form-control" id="price" name="input-price" type="text" value="{{old('input-price')}}" placeholder="Price">
                      <span class="my-form-error">{{$errors->first('input-price')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="fuel-type">Fuel Type</label>
                      <select class="form-control" name="input-fuel-type" id="fuel-type">
                      <option value="">Select Fuel Type</option>
                      @foreach($data['all_fuel_type'] as $fuel_types)
                        <option value="{{$fuel_types->id}}"{{(old('input-fuel-type') == $fuel_types->id) ? 'selected' : ''}}>{{ucfirst($fuel_types->fuel_type)}}</option>
                      @endforeach
                      </select>
                      <span class="my-form-error">{{$errors->first('input-fuel-type')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="transmission">Transmission Type</label>
                      <select name="input-transmission" id="transmission" class="form-control">
                        <option value="">Select Transmission</option>
                        @foreach($data['all_transmission_type'] as $transmission_types)
                          <option value="{{$transmission_types->id}}"{{(old('input-transmission') == $transmission_types->id) ? 'selected' : ''}}>{{ucfirst($transmission_types->transmission_type)}}</option>
                        @endforeach
                      </select>
                      <span class="my-form-error">{{$errors->first('input-price')}}</span>
                    </div>
                  </div>
                </div>
                <div class="heading gray no-padding-lr">
                  <i class="fa fa-file-text-o"></i>Basic Info
                </div>
                <div class="heading-info">
                  <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="wheels" class="control-label">Number of Wheels</label>
                      <select class="form-control" name="input-wheels" id="wheels">
                        <option value="">Select no. of Wheels</option>
                        @foreach($data['all-wheel'] as $wheels)
                          <option value="{{$wheels->id}}"{{(old('input-wheels') == $wheels->id) ? 'selected' : ''}}>{{$wheels->wheel_count}}</option>
                        @endforeach
                      </select>
                      <span class="my-form-error">{{$errors->first('input-wheels')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="conditions" class="control-label">Vehicle Condition</label>
                      <select class="form-control" name="input-condition" id="conditions">
                        <option value="">Select condition</option>
                        @foreach($data['all-condition'] as $conditions)
                        <option value="{{$conditions->id}}"{{(old('input-condition') == $conditions->id) ? 'selected' : ''}}>{{ucfirst($conditions->condition)}}</option>
                        @endforeach
                      </select>
                      <span class="my-form-error">{{$errors->first('input-condition')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="product-for" class="control-label">For Sale or Rent </label>
                      <select class="form-control" name="input-product-for" id="product-for">
                        <option value="">Sale or Rent</option>
                        @foreach($data['all-type'] as $types)
                        <option value="{{$types->id}}"{{(old('input-product-for') == $types->id) ? 'selected' : ''}}>{{ucfirst($types->product_type)}}</option>
                        @endforeach
                      </select>
                      <span class="my-form-error">{{$errors->first('input-product-for')}}</span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="mileage">Mileage</label>
                      <input type="text" class="form-control" name="input-milage" placeholder="Mileage (in Km)">
                      <span class="my-form-error">{{$errors->first('input-milage')}}</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="release-date">Release Year</label>
                      <select class="form-control" name="input-release-year" id="release-date">
                        <option value="">Select Release Year</option>
                        @foreach($data['all_release_date'] as $release_date)
                        <option value="{{$release_date->id}}"{{(old('input-release-year') == $release_date->id) ? 'selected' : ''}}>{{$release_date->product_release_date}}</option>
                        @endforeach
                      </select>
                      <span class="my-form-error">{{$errors->first('input-release-year')}}</span>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
@stop