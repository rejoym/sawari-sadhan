@extends('backend._shared.layouts.master')
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container">
      <div class="heading">
        <i class="fa fa-shield"></i>Add New Brand
      </div>
      <div class="my-widget-content">
        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil impedit molestiae illo. Aspernatur quasi, sed commodi, a aliquam distinctio voluptate!</span>
      </div>
      <div class="row widget-alert-wrapper">
        <div class="widget-alert col-md-6">
          @if(Session::get('Success'))
            <div class="alert alert-success my-widget-alert" role="alert">
              <i class="fa fa-check"></i> Success! New brand has been successfully registered.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          @if($errors->has('input-brand-name'))
            <div class="alert alert-danger my-widget-alert" role="alert">
              <i class="fa fa-exclamation-triangle"></i> Error! Brand Name field is required!
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
        </div>
      </div>
      <div class="widget-content padded">
        <form action="" method="post">
          {!! csrf_field() !!}
          <fieldset>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="brandname">Brand Name *</label><input class="form-control" id="brandname" name="input-brand-name" type="text" placeholder="Enter Brand Name">
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <!-- Striped Table -->
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix my-container">
      <div class="heading">
        <i class="fa fa-table"></i>All Available Brands
      </div>
      <div class="widget-content padded clearfix">
        <table class="table table-striped" id="datatable-editable">
          <thead>
            <th>S-N</th>
            <th>BRAND</th>
            <th>ACTION</th>
          </thead>
          <tbody>
            <?php $sn = 0; ?>
            @foreach($data['all_brand'] as $brands)
            <tr>
              <td>
                {{++$sn}}
              </td>
              <td>
                {{ucfirst($brands->brand_name)}}
              </td>
              <td>
                <a href="#" class="record-edit"><i class="fa fa-pencil"></i> Edit</a> &nbsp;|&nbsp; <a href="#" class="record-delete"><i class="fa fa-trash-o"></i> Delete</a>
              </td>
              @endforeach
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@stop