@extends('backend._shared.layouts.master')
@section('main-content')
<div class="row">
  <!-- Striped Table -->
  <div class="col-lg-12">
    @if(Session::get('Success'))
            <div class="alert alert-success my-widget-alert" role="alert">
              <i class="fa fa-check"></i> Success! New product has been successfully added.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
    <div class="widget-container fluid-height clearfix my-container">
      <div class="widget-content padded clearfix">
        <table class="table table-striped" id="datatable-editable">
          <thead>
            <th>S-N</th>
            <th>Model</th>
            <th>Brand</th>
            <th>Variant</th>
            <th>No. of Wheels</th>
            <th>Vehicle Condition</th>
            <th>Type(Sale or rent)</th>
            <th>Fuel</th>
            <th>Transmission</th>
            <th>Price</th>
            <th>Release Year</th>
            <th>Mileage</th>
            <th>Action</th>
          </thead>
          <tbody>
            <?php $sn = 0; ?>
            @foreach($data['all_product'] as $products)
            <tr>
              <td>
                {{++$sn}}
              </td>
              <td>
                {{ucfirst($products->modal->modal_name)}}
              </td>
              <td class="hidden-xs">
                {{ucfirst($products->brand->brand_name)}}
              </td>
              <td>
                {{ucfirst($products->variant->variant_name)}}
              </td>
              <td>
                {{ucfirst($products->wheel->wheel_count)}}
              </td>
              <td>
                {{ucfirst($products->condition->condition)}}
              </td>
              <td>
                {{ucfirst($products->productType->product_type)}}
              </td>
              <td>
                {{ucfirst($products->fuel->fuel_type)}}
              </td>
              <td>
                {{ucfirst($products->transmission->transmission_type)}}
              </td>
              <td>
                {{$products->price}}
              </td>
              <td>
                {{ucfirst($products->productReleaseDate->product_release_date)}}
              </td>
              <td>
                {{ucfirst($products->milage)}}
              </td>
              <td>
                <a href="#" class="record-edit"><i class="fa fa-pencil"></i> Edit</a> &nbsp;|&nbsp; <a href="#" class="record-delete"><i class="fa fa-trash-o"></i> Delete</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop