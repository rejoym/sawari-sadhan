@extends('backend._shared.layouts.master')
@section('main-content')
<div class="row">
  <!-- Striped Table -->
  <div class="col-lg-12">
    @if(Session::get('Success'))
    <div class="alert alert-success my-widget-alert" role="alert">
      <i class="fa fa-check"></i> Success! Exterior has been successfully added.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    <div class="widget-container fluid-height clearfix my-container">
      <div class="widget-content padded clearfix">
        <table class="table table-striped" id="datatable-editable">
          <thead>
            <th>S-N</th>
            <th>Product</th>
            <th>Alloy Wheel</th>
            <th>Skid Plate</th>
            <th></th>
            <th>Daynight Rear View MIrror</th>
            <th>Childseat Anchor</th>
            <th>Immobilizer</th>
            <th>Front Fog Lamps</th>
            <th>Rear Defogger With Timer</th>
            <th>Automatic Head Lamps</th>
            <th>Door</th>
            <th>Tailgate</th>
            <th>Smart Pedal</th>
            <th>Clutch Lock</th>
            <th>Audio Video Navigation</th>
            <th>Rear View Camera</th>
            <th>Headlight Escort Function</th>
            <th>Seatbelt Pretensioners</th>
            <th>Isofix</th>
            <th>Dual Horn</th>
            <th>Action</th>
          </thead>
          <tbody>
            <?php $sn = 0; ?>
            @foreach($data['all_safety'] as $safeties)
            <tr>
              <td>
                {{++$sn}}
              </td>
              <td>
                {{ucwords($safeties->product->brand->brand_name .' - ' .$safeties->product->modal->modal_name .' - ' .$safeties->product->variant->variant_name)}}
              </td>
              <td>
                {{($safeties->electronic_brakeforce_distribution) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->electronic_stability_control) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->vehicle_stability_management_control) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->day_night_rear_view_mirror) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->child_seat_anchor) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->immobilizer) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->front_fog_lamps) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->rear_defogger_with_timer) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->automatic_head_lamps) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->centralLocking->door) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->centralLocking->tailgate) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->smart_pedal) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->clutch_lock) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->audioVideoNavigation->avn) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->audioVideoNavigation->avn_rear_view_camera_display) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->headlight_escort_function) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->seat_belt_pretensioners) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->isofix) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                {{($safeties->dual_horn) == 1 ? 'Yes' : 'No'}}
              </td>
              <td>
                <a href="#" class="record-edit"><i class="fa fa-pencil"></i> Edit</a> &nbsp;|&nbsp; <a href="#" class="record-delete"><i class="fa fa-trash-o"></i> Delete</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop