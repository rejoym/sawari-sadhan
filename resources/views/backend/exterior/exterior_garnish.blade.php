<div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="garnish-location">Garnish Location</label>
                  <select class="form-control" name="input-garnish-location" id="garnish-location" multiple="">
                    <option>Front</option>
                    <option>Rear</option>
                    <option>Side</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="garnish-type">Garnish type</label>
                  <select id="garnish-type" name="input-garnish-type" class="form-control">
                    <option>Garnish 1</option>
                    <option>Garnish 2</option>
                    <option>Garnish 3</option>
                  </select>
                </div>
              </div>
            </div>