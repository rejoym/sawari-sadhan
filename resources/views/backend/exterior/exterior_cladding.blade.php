<div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cladding-location">Cladding Location</label>
                  <select class="form-control" name="input-cladding-location" id="cladding-location" multiple="">
                    <option>Front</option>
                    <option>Rear</option>
                    <option>Side</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="cladding-type">Cladding type</label>
                  <select id="cladding-type" name="input-cladding-type" class="form-control">
                    <option>Wood</option>
                    <option>Timber</option>
                  </select>
                </div>
              </div>
            </div>