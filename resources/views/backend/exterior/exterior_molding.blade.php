<div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="molding-location">Molding Location</label>
                  <select class="form-control" name="input-molding-location" id="molding-location" multiple="">
                    <option>Front</option>
                    <option>Rear</option>
                    <option>Side</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="molding-type">Molding type</label>
                  <select id="molding-type" name="input-molding-type" class="form-control">
                    <option>Plastic</option>
                    <option>Steel</option>
                  </select>
                </div>
              </div>
            </div>