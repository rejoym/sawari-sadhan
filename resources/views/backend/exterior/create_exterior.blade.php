@extends('backend._shared.layouts.master')
@section('main-content')
<?php $choices = [
  '1' => 'Yes',
  '0' => 'No'
];
?>
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="widget-content padded">
        <form action="" id="validate-form" method="post" class="product-form">
          {!! csrf_field() !!}
          <fieldset>
            <div class="heading no-padding-lr">
              <i class="fa fa-align-left"></i>Add Exterior
            </div>
            <div class="heading no-padding-lr gray">
              <i class="fa fa-file-text-o"></i>Exterior Specifications
            </div>
            <div class="heading-info">
              <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="exterior-product">Select Product</label>
                  <select name="input-exterior-product" id="exterior-product" class="form-control">
                    <option value="">Select Product</option>
                    @foreach($data['all_product'] as $products)
                      <option value="{{$products->id}}"{{(old('input-safety-product')== $products->id) ? 'selected' : ''}}>{{ucwords($products->brand->brand_name .' - ' .$products->modal->modal_name .' - ' .$products->variant->variant_name)}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="alloy-wheel-type">Alloy Wheel Type</label>
                  <select class="form-control" id="alloy-wheel-type" name="input-alloy-wheel">
                    <option>Select Alloy Wheels</option>
                    @foreach($data['all_alloy_wheel'] as $alloy_wheels)
                    <option value="{{$alloy_wheels->id}}">{{ucfirst($alloy_wheels->alloy_wheel_type)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="front-skid-plate">Front Skid Plate</label>
                  <select class="form-control" id="front-skid-plate" name="input-front-skid">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="rear-skid">Rear Skid Plate</label>
                  <select class="form-control" id="rear-skid" name="input-rear-skid">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="radiator-grill">Radiator Grill</label>
                  <select name="input-radiator-grill" id="radiator-grill" class="form-control">
                    <option>Select Radiator Grill</option>
                    @foreach($data['all_radiator_grill'] as $radiator_grill)
                    <option value="{{$radiator_grill->id}}">{{ucfirst($radiator_grill->radiator_grill)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="roof-rails">Roof Rails</label>
                  <select name="input-roof" id="roof-rails" class="form-control">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="step-plate">Step Plate</label>
                  <select name="input-step-plate" id="step-plate" class="form-control">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="doorhandle-finish">Exterior Doorhandle Finish</label>
                  <select class="form-control" name="input-doorhandle-finish" id="doorhandle-finish">
                    <option value="">Select Doorhandle Finish</option>
                    @foreach($data['all_door_finish'] as $doors)
                      <option value="{{$doors->id}}"{{(old('input-doorhandle-finish') == $doors->id) ? 'selected' : ''}}>{{ucfirst($doors->doorhandle_finish)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="mirror-indicator">Mirror Indicator</label>
                  <select id="mirror-indicator" name="input-mirror-indicator" class="form-control">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                      <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="clutch-footrest">Clutch Footrest</label>
                  <select id="clutch-footrest" name="input-clutch-footrest" class="form-control">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="antenna">Antenna</label>
                  <select class="form-control" name="input-antenna" id="antenna">
                    <option>Select Antenna</option>
                    @foreach($data['all_antenna'] as $antennas)
                      <option value="{{$antennas->id}}"{{(old('input-antenna') == $antennas->id) ? 'selected' : ''}}>{{ucfirst($antennas->antenna)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>

@stop