 <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="lamp-type">Lamp type</label>
                  <select id="lamp-type" name="input-lamp-type" class="form-control">
                    <option>Cornering</option>
                    <option>Positioning</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="lamp-location">Lamp Location</label>
                  <select class="form-control" name="input-lamp-location" id="lamp-location" multiple="">
                    <option>Front</option>
                    <option>Rear</option>
                    <option>Side</option>
                  </select>
                </div>
              </div>
            </div>