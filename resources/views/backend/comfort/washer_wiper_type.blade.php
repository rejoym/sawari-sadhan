@extends('backend._shared.layouts.master')
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="row widget-alert-wrapper product-alerts">
        <div class="widget-alert col-md-12 variant-widget-alert">
          @if(Session::get('Success'))
          <div class="alert alert-success my-widget-alert" role="alert">
            <i class="fa fa-check"></i> Success! New Washer Wiper Type has been successfully added.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          @if($errors->has('input-product-washer') || $errors->has('input-comfort-washer'))
          <div class="alert alert-danger my-widget-alert" role="alert">
            <i class="fa fa-exclamation-triangle"></i> Error! Form Validation Error! Please try again.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
        </div>
      </div>
      <div class="widget-content padded">
        <form action="" id="validate-form" method="post" class="product-form">
          {!! csrf_field() !!}
          <fieldset>
            <div class="heading">
              <i class="fa fa-file-text-o"></i>Comfort - Washer Wiper
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="product-washer">Select Product</label>
                  <select class="form-control" id="product-washer" name="input-product-washer">
                    <option>Select Product</option>
                    @foreach($data['all_product'] as $products)
                    <option value="{{$products->id}}"{{(old('input-product-washer')== $products->id) ? 'selected' : ''}}>{{ucwords($products->brand->brand_name .' - ' .$products->modal->modal_name .' - ' .$products->variant->variant_name)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="washer-type">Washer Wiper Type</label>
                  <select class="form-control" multiple="" id="washer-type" name="input-comfort-washer[]">
                    @foreach($data['all_washer_type'] as $washers)
                    <option value="{{$washers->id}}">{{ucfirst($washers->washer_wiper_type)}}</option>
                    @endforeach
                  </select>

                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
          </fieldset>
        </form>
          </div>
        </div>
      </div>
    </div>

@stop