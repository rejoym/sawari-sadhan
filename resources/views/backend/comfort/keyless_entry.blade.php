@extends('backend._shared.layouts.master')
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="widget-content padded">
        <form action="" id="validate-form" method="post" class="product-form">
          {!! csrf_field() !!}
          <fieldset>
            <div class="heading">
              <i class="fa fa-file-text-o"></i>Comfort - Keyless Entry
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="safety-product">Select Product</label>
                  <select class="form-control" id="safety-product" name="input-product-keyless">
                    <option>Select Product</option>
                    @foreach($data['all_product'] as $products)
                    <option value="{{$products->id}}"{{(old('input-safety-product')== $products->id) ? 'selected' : ''}}>{{ucwords($products->brand->brand_name .' - ' .$products->modal->modal_name .' - ' .$products->variant->variant_name)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="keyless-entry-type">Keyless Entry Type</label>
                  <select class="form-control" multiple="" id="keyless-entry-type" name="input-keyless-entry[]">
                    @foreach($data['all_keyless_entry'] as $entries)
                    <option value="{{$entries->id}}">{{ucfirst($entries->keyless_entry_type)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
          </fieldset>
        </form>
          </div>
        </div>
      </div>
    </div>

@stop