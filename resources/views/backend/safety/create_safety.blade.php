@extends('backend._shared.layouts.master')
@section('main-content')
<?php
$choices = [
  '1' => 'Yes',
  '0' => 'No'
];
?>
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="widget-content padded">
        <form action="" id="validate-form" method="post" class="product-form">
          {!! csrf_field() !!}
          <fieldset>
            <div class="heading no-padding-lr">
              <i class="fa fa-align-left"></i>Add New Safety
            </div>
            <div class="heading no-padding-lr gray">
              <i class="fa fa-file-text-o"></i>Safety Specifications
            </div>
            <div class="heading-info">
              <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="safety-product">Select Product</label>
                  <select class="form-control" id="safety-product" name="input-safety-product">
                    <option>Select Product</option>
                    @foreach($data['all_products'] as $products)
                    <option value="{{$products->id}}"{{(old('input-safety-product')== $products->id) ? 'selected' : ''}}>{{ucwords($products->brand->brand_name .' - ' .$products->modal->modal_name .' - ' .$products->variant->variant_name)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="electronic-brake-dist">Electronic Brakeforce Distribution</label>
                  <select class="form-control" id="electronic-brake-dist" name="input-elec-brake-dist">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}"{{(old('input-elec-brake-dist') == $key) ? 'selected' : ''}}>{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="electronic-stability-control">Electronic Stability Control</label>
                  <select class="form-control" id="electronic-stability-control" name="input-elec-stability-ctrl">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="vehicle-stability-mgmt-control">Vehicle Stability Management Control</label>
                  <select class="form-control" id="vehicle-stablity-mgmt-control" name="input-vehicle-stab-ctrl">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="day-night-rear">Day Night Rear View Mirror</label>
                  <select class="form-control" id="day-night-rear" name="input-day-night-mirror">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="child-seat-anchor">Child Seat Anchor</label>
                  <select class="form-control" id="child-seat-anchor" name="input-child-seat">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="immobilizer">Immobilizer</label>
                  <select class="form-control" id="immobilizer" name="input-immobilizer">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="front-fog-lamps">Front Fog Lamps</label>
                  <select class="form-control" id="front-fog-lamps" name="input-front-fog">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="rear-defogger-with-timer">Rear defogger with timer</label>
                  <select class="form-control" id="rear-defogger-with-timer" name="input-rear-defogger">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="automatic-head-lamps">Automatic Head Lamps</label>
                  <select class="form-control" id="automatic-head-lamps" name="input-automatic-headlamps">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="smart-pedal">Smart Pedal</label>
                  <select class="form-control" id="smart-pedal" name="input-smart-pedal">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="clutch-lock">Clutch Lock</label>
                  <select class="form-control" id="clutch-lock" name="input-clutch-lock">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="headlight-esc-function">Headlight Escort Function</label>
                  <select class="form-control" id="headlight-esc-function" name="input-headlight-func">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="seatbelt">Seat Belt Pretensioners</label>
                  <input class="form-control" type="text" name="input-seatbelt" id="seatbelt" placeholder="Seat Belt Pretensioners">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="isofix">Isofix</label>
                  <select class="form-control" id="isofix" name="input-isofix">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="dualhorn">Dual Horn</label>
                  <select class="form-control" id="dualhorn" name="input-dualhorn">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="heading no-padding-lr gray">
              <i class="fa fa-file-text-o"></i>Central Locking
            </div>
            <div class="heading-info">
              <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="door">Door</label>
                  <select class="form-control" id="door" name="input-door">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="tailgate">Tailgate</label>
                  <select class="form-control" id="tailgate" name="input-tailgate">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <div class="heading no-padding-lr gray">
              <i class="fa fa-file-text-o"></i>Audio Video Navigation
            </div>
            <div class="heading-info">
              <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="avn">Audio Video Navigation</label>
                  <select class="form-control" id="avn" name="input-avn">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="rear-view-camera-display">Rear View Camera Display</label>
                  <select class="form-control" id="rear-view-camera-display" name="input-rear-view-camera">
                    <option>Select Yes/No</option>
                    @foreach($choices as $key=>$value)
                    <option value="{{$key}}">{{$value}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
@stop