<div class="heading">
                      <i class="fa fa-file-text-o"></i>Interior
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="rear-parcel-tray">Rear Parcel Tray</label>
                          <select class="form-control" name="input-rear-parcel" id="rear-parcel-tray">
                            <option>Select Yes/No</option>
                            <option>Yes</option>
                            <option>No</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="rear-seat-armrest">Rear Seat Center Armrest</label>
                          <select id="rear-seat-armrest" name="input-rear-seat-armrest" class="form-control">
                            <option>Select Yes/No</option>
                            <option>Yes</option>
                            <option>No</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="interior-doorhandle-finish">Door Handle Finish</label>
                          <select id="interior-doorhandle-finish" name="input-interior-doorhandle" class="form-control">
                            <option>Select Doorhandle Finish</option>
                            <option>Chrome</option>
                            <option>Leather</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <!--sub heading-->
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="steering-wrapping">Steering Wheel Wrapping</label>
                          <select id="steering-wrapping" name="input-steering" class="form-control">
                            <option>Select Wrapping</option>
                            <option>Leather</option>
                            <option>Nylon</option>
                            <option>Carbon</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label for="steering-coating">Steering Wheel Coating</label>
                        <select id="steering-coating" name="input-steering-coating" class="form-control">
                          <option>Select Coating</option>
                          <option>Zinc</option>
                          <option>Lead</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="gear-knob-wrapping">Gear Knob Wrapping</label>
                          <select id="gear-knob-wrapping" name="input-gear-wrapping" class="form-control">
                            <option>Select Wrapping</option>
                            <option>Leather</option>
                            <option>Nylon</option>
                            <option>Carbon</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <label for="gear-knob-coating">Gear Knob Coating</label>
                        <select id="gear-knob-coating" name="input-gear-coating" class="form-control">
                          <option>Select Coating</option>
                          <option>Zinc</option>
                          <option>Lead</option>
                        </select>
                      </div>
                    </div>
                    <div class="heading">
                      <i class="fa fa-file-text-o"></i>Seating
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="driver-seat-height">Driver Seat Height Adjustable</label>
                          <select id="driver-seat-height" name="input-driver-seat" class="form-control">
                            <option>Select Yes/No</option>
                            <option>Yes</option>
                            <option>No</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="front-seat-headrest">Front Seat Headrest</label>
                          <select id="front-seat-headrest" name="input-seat-headrest" class="form-control">
                            <option>Select Doorhandle Finish</option>
                            <option>Chrome</option>
                            <option>Leather</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="rear-seat-headrest">Rear Seat Headrest</label>
                          <select class="form-control" name="input-rear-headrest" id="rear-seat-headrest">
                            <option>Select Yes/No</option>
                            <option>Yes</option>
                            <option>No</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="info-conn-sys">Infotainment Connectivity System Device</label>
                          <select id="info-conn-sys" name="input-info-conn-sys" class="form-control">
                            <option>Select Device</option>
                            <option>Device 1</option>
                            <option>Device 2</option>
                            <option>Device 3</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="info-conn-smart">Infotainment Connectivity Smartphone Specifications</label>
                          <select id="info-conn-smart" name="input-info-conn-smart" class="form-control">
                            <option>Select Specifications</option>
                            <option>Specification 1</option>
                            <option>Specification 2</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>