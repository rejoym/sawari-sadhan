<!DOCTYPE html>
<html>
<head>
  <title>
    Sawari Sadhan - Dashboard
  </title>
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/hightop-font.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/isotope.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/jquery.fancybox.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/fullcalendar.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/wizard.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/select2.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/morris.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/datatables.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/datepicker.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/timepicker.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/colorpicker.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/bootstrap-switch.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/bootstrap-editable.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/daterange-picker.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/typeahead.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/summernote.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/ladda-themeless.min.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/social-buttons.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/jquery.fileupload-ui.css') }}" media="screen" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/dropzone.css') }}" media="screen" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/nestable.css') }}" media="screen" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/pygments.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
   <link href="{{ asset('public/assets/backend/css/ionicons.min.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="https://unpkg.com/ionicons@4.2.2/dist/css/ionicons.min.css" rel="stylesheet">
  <link href="{{ asset('public/assets/backend/css/custom.css') }}" media="all" rel="stylesheet" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/color/green.css') }}" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/color/orange.css') }}" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
  <link href="{{ asset('public/assets/backend/css/color/magenta.css') }}" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
  <!-- <meta name="_base_url" content="{{ url('/') }}"> -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
</head>
<body class="page-header-fixed bg-1">
  <div class="modal-shiftfix main-body-wrapper">
    <!-- Navigation -->
    <div class="navbar navbar-fixed-top scroll-hide">
      <div class="container-fluid top-bar">
        <div class="pull-right">
          <ul class="nav navbar-nav pull-right">
            <li class="dropdown notifications hidden-xs">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span aria-hidden="true" class="hightop-flag"></span>
                <div class="sr-only">
                  Notifications
                </div>
                <p class="counter">
                  4
                </p>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#">
                  <div class="notifications label label-success">
                    New
                  </div>
                  <p>
                    New user added: Jane Smith
                  </p></a>

                </li>
                <li><a href="#">
                  <div class="notifications label label-success">
                    New
                  </div>
                  <p>
                    Sales targets available
                  </p></a>

                </li>
                <li><a href="#">
                  <p>
                    New performance metric added
                  </p></a>

                </li>
                <li><a href="#">
                  <div class="notifications label label-success">
                    New
                  </div>
                  <p>
                    New growth data available
                  </p></a>

                </li>
              </ul>
            </li>
            <li class="dropdown messages hidden-xs">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span aria-hidden="true" class="hightop-envelope"></span>
                <div class="sr-only">
                  Messages
                </div>
                <p class="counter">
                  3
                </p>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#">
                  <img width="34" height="34" src="{{ asset('public/assets/backend/images/avatar-male2.png') }}" />Could we meet today? I wanted...</a>
                </li>
                <li><a href="#">
                  <img width="34" height="34" src="{{ asset('public/assets/backend/images/avatar-female.png') }}" />Important data needs your analysis...</a>
                </li>
                <li><a href="#">
                  <img width="34" height="34" src="{{ asset('public/assets/backend/images/avatar-male2.png') }}" />Buy Se7en today, it's a great theme...</a>
                </li>
              </ul>
            </li>
            <li class="dropdown user hidden-xs"><a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <img width="34" height="34" src="{{ asset('public/assets/backend/images/avatar-male.jpg') }}" />{{ Auth::user()->username }}<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">
                  <i class="fa fa-user"></i>My Account</a>
                </li>
                <li><a href="{{ route('get.logout') }}">
                  <i class="fa fa-sign-out"></i>Logout</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <button class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="logo logo-title" target="_blank" href="{{ url('/') }}"><span class="red">Sawari</span> Sadhan <span class="sub-title">&nbsp;"Admin"</span></a>
      </div>
        @if(Auth::user()->user_type == 'admin')
        <div class="container-fluid main-nav clearfix">
          <div class="nav-collapse">
            <ul class="nav main-b-menu">
              <li>
                <a class="{{ Helper::setActiveMenu('dashboard') }}" href="{{ url('admin/dashboard') }}"><i class="ion-md-home admin-menu-icon"></i>Dashboard</a>
              </li>
              <li class="dropdown">
              <a data-toggle="dropdown" href="#" class="{{ Helper::setActiveMenu('components') }}">
                <i class="ion-ios-apps admin-menu-icon"></i>Components<b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="#">Wheels</a>
                </li>
                <li>
                  <a href="#">Condition</a>
                </li>
                <li>
                  <a href="#">Fuel</a>
                </li>
                <li>
                  <a href="{{route('get.location.create')}}">Location</a>
                </li>
                <li>
                  <a href="">Molding </a>
                </li>
                <li>
                  <a href="{{route('get.antenna.create')}}">Antenna</a>
                </li>
                <li>
                  <a href="{{route('get.alloywheel.create')}}">Alloy Wheel</a>
                </li>
                <li>
                  <a href="{{route('get.radiator.create')}}">Radiator Grill</a>
                </li>
                <li>
                  <a href="{{route('get.doorhandle.create')}}">Doorhandle Finish</a>
                </li>
                <li>
                  <a href="{{route('get.bodypart.create')}}">Body Part</a>
                </li>
                <li>
                  <a href="{{route('get.bodypartcolor.create')}}">Body Part Color</a>
                </li>
                <li>
                  <a href="{{route('get.lamptype.create')}}">Lamp Type</a>
                </li>
                <li>
                  <a href="{{route('get.lamps.create')}}">Lamps</a>
                </li>
                <li>
                  <a href="{{route('get.garnishtype.create')}}">Garnish Type</a>
                </li>
                <li>
                  <a href="{{route('get.garnish.create')}}">Garnish</a>
                </li>
                 <li>
                  <a href="{{route('get.moldingtype.create')}}">Molding Type</a>
                </li>
                <li>
                  <a href="#">Molding</a>
                </li>
              </ul>
            </li>
              <li class="dropdown">
                <a data-toggle="dropdown" href="#" class="{{ Helper::setActiveMenu('product') }}">
                <i class="ion-ios-car admin-menu-icon"></i>
                Products<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="{{route('get.brand.list')}}">Brands</a>
                  </li>
                  <li class="sub-menu-parent">
                    <a href="{{route('get.model.list')}}">Models</a>
                  </li>
                  <li>
                    <a href="{{route('get.variant.list')}}">Variants</a>
                  </li>
                  <li class="sub-menu-parent">
                  <a href="#" class="sub-menu-parent-item">Product <i class="fa fa-angle-double-right sub-menu-arrow"></i></a>
                  <div class="sub-menu-wrapper">
                    <ul class="sub-menu">
                      <li class="sub-menu-item"><a href="{{route('get.product.create')}}">Add Product</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.product.list')}}">List Product</a></li>
                      <li class="sub-menu-item"><a href="#">Product Images</a></li>
                    </ul>
                  </div>

                </li>
                <li class="sub-menu-parent">
                  <a href="#" class="sub-menu-parent-item">Safety <i class="fa fa-angle-double-right sub-menu-arrow"></i></a>
                  <div class="sub-menu-wrapper">
                    <ul class="sub-menu">
                      <li class="sub-menu-item"><a href="{{route('get.safety.create')}}">Add Safety</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.safety.list')}}">List Safety</a></li>
                      <li class="sub-menu-item"><a href="#">Braking System</a></li>
                      <li class="sub-menu-item"><a href="#">Parking Assist</a></li>
                      <li class="sub-menu-item"><a href="#">Start Assist</a></li>
                      <li class="sub-menu-item"><a href="#">Body Structure</a></li>
                      <li class="sub-menu-item"><a href="#">Air Bag</a></li>
                    </ul>
                  </div>

                </li>
                <li class="sub-menu-parent">
                  <a href="#" class="sub-menu-parent-item">Comfort <i class="fa fa-angle-double-right sub-menu-arrow"></i></a>
                  <div class="sub-menu-wrapper">
                    <ul class="sub-menu">
                      <li class="sub-menu-item"><a href="{{route('get.comfort.keyless')}}">Keyless Entry</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.aircondition')}}">Air Condition</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.mirror')}}">Mirror</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.powerwindow')}}">Power Window</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.washerwiper')}}">Washer Wiper</a></li>
                    </ul>
                  </div>
                </li>
                <li class="sub-menu-parent">
                  <a href="#" class="sub-menu-parent-item">Exterior <i class="fa fa-angle-double-right sub-menu-arrow"></i></a>
                  <div class="sub-menu-wrapper">
                    <ul class="sub-menu">
                      <li class="sub-menu-item"><a href="{{route('get.exterior.create')}}">Add Exterior</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.exterior.list')}}">List Exterior</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.keyless')}}">Exterior Body Part Color</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.aircondition')}}">Exterior Body Part Finish</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.mirror')}}">Lamp</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.powerwindow')}}">Molding</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.washerwiper')}}">Cladding</a></li>
                      <li class="sub-menu-item"><a href="{{route('get.comfort.washerwiper')}}">Garnish</a></li>
                    </ul>
                  </div>
                </li>
                  <li>
                    <a href="#">Interior</a>
                  </li>
                  <li>
                    <a href="#">Instrument</a>
                  </li>
                  <li>
                    <a href="#">Seating</a>
                  </li>
                  <li>
                    <a href="#">Infotainment Connectivity</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown">
                <a data-toggle="dropdown" href="#">
                  <i class="ion-md-contacts admin-menu-icon"></i>
                  Users<b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Add New Users</a>
                  </li>
                  <li>
                    <a href="#">List/Edit Users</a>
                  </li>
                </ul>
              </li>
              <li class="dropdown">
                <a data-toggle="dropdown" href="#">
                  <i class="ion-ios-list admin-menu-icon"></i>Blogs<b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Add New Blog</a>
                  </li>
                  <li>
                    <a href="#">Test Blog</a>
                  </li>
                </ul>
              </li>
              <li>
                <a class="" href="#"><i class="ion-ios-swap admin-menu-icon"></i>Support</a>
              </li>
              <li>
                <a class="" href="#">
                  <i class="ion-ios-git-compare admin-menu-icon"></i>
                  Routing
                </a>
              </li>
              <li class="dropdown"><a data-toggle="dropdown" href="#">
                <i class="ion-ios-cog admin-menu-icon"></i>
                Settings<b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#">Profile</a>
                  </li>
                  <li>
                    <a href="#">Administrators</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      @else
      <div class="container-fluid main-nav clearfix">
        <div class="nav-collapse">
          <ul class="nav">
            <li>
              <a class="current" href="index.html"><span aria-hidden="true" class="hightop-home"></span>Dashboard</a>
            </li>
          </ul>
        </div>
      </div>
      @endif
    </div>