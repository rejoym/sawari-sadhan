@extends('backend._shared.layouts.master')
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="row widget-alert-wrapper">
        <div class="widget-alert col-md-12">
          @if(Session::get('Success'))
            <div class="alert alert-success my-widget-alert" role="alert">
              <i class="fa fa-check"></i> Success! New Radiator Grill has been successfully registered.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          @if($errors->has('input-radiator-grill'))
            <div class="alert alert-danger my-widget-alert" role="alert">
              <i class="fa fa-exclamation-triangle"></i> Error! Radiator Grill field is required!
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
        </div>
      </div>
      <div class="widget-content padded">
        <form action="" id="validate-form" method="post" class="product-form">
          {!! csrf_field() !!}
          <fieldset>
            <div class="heading no-padding-lr">
              <i class="fa fa-align-left"></i>Add Radiator Grill
            </div>
            <div class="heading no-padding-lr gray">
              <i class="fa fa-file-text-o"></i>Radiator Grill specifications
            </div>
            <div class="heading-info">
              <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="radiator">Radiator Grill</label>
                  <input class="form-control" name="input-radiator-grill" id="radiator" value="{{old('input-radiator-grill')}}" placeholder="Radiator Grill" }}>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <!-- Striped Table -->
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix my-container">
      <div class="heading">
        <i class="fa fa-table"></i>All Available Radiator Grill
      </div>
      <div class="widget-content padded clearfix">
        <table class="table table-striped" id="datatable-editable">
          <thead>
            <th>S-N</th>
            <th>RADIATOR GRILL</th>
            <th>ACTION</th>
          </thead>
          <tbody>
            <?php $sn = 0; ?>
            @foreach($data['all_radiator_grill'] as $grills)
            <tr>
              <td>
                {{++$sn}}
              </td>
              <td>
                {{ucfirst($grills->radiator_grill)}}
              </td>
              <td>
                <a href="#" class="record-edit"><i class="fa fa-pencil"></i> Edit</a> &nbsp;|&nbsp; <a href="#" class="record-delete"><i class="fa fa-trash-o"></i> Delete</a>
              </td>
              @endforeach
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop