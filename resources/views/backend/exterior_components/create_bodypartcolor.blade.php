@extends('backend._shared.layouts.master')
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="row widget-alert-wrapper">
        <div class="widget-alert col-md-12">
          @if(Session::get('Success'))
            <div class="alert alert-success my-widget-alert" role="alert">
              <i class="fa fa-check"></i> Success! New Body part color has been successfully registered.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          @if($errors->has('input-body-part') || $errors->has('input-color'))
            <div class="alert alert-danger my-widget-alert" role="alert">
              <i class="fa fa-exclamation-triangle"></i> Error! Form field is required!
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
        </div>
      </div>
      <div class="widget-content padded">
        <form action="" id="validate-form" method="post" class="product-form">
          {!! csrf_field() !!}
          <fieldset>
            <div class="heading no-padding-lr">
              <i class="fa fa-align-left"></i>Add Body Part Color
            </div>
            <div class="heading no-padding-lr gray">
              <i class="fa fa-file-text-o"></i>Body Part Color specifications
            </div>
            <div class="heading-info">
              <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="bodypart">Body Part</label>
                  <select class="form-control" name="input-body-part" id="bodypart" value="{{old('input-body-part')}}">
                    <option value="">Select Body Part</option>
                    @foreach($data['all_body_part'] as $parts)
                      <option value="{{$parts->id}}"{{(old('input-parts') == $parts->id) ? 'selected' : ''}}>{{ucfirst($parts->body_part)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="color">Color</label>
                  <select class="form-control" name="input-color" id="color">
                    <option value="">Select Color</option>
                    @foreach($data['all_color'] as $colors)
                      <option value="{{$colors->id}}"{{(old('input-colors') == $colors->id) ? 'selected' : ''}}>{{ucfirst($colors->color_name)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <!-- Striped Table -->
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix my-container">
      <div class="heading">
        <i class="fa fa-table"></i>All Available Body Part Colors
      </div>
      <div class="widget-content padded clearfix">
        <table class="table table-striped" id="datatable-editable">
          <thead>
            <th>S-N</th>
            <th>BODY PART</th>
            <th>COLOR</th>
            <th>ACTION</th>
          </thead>
          <tbody>
            <?php $sn = 0; ?>
            @foreach($data['all_body_part_color'] as $bodypartcolors)
            <tr>
              <td>
                {{++$sn}}
              </td>
              <td>
                {{ucfirst($bodypartcolors->bodyPart->body_part)}}
              </td>
              <td>
                {{ucfirst($bodypartcolors->color->color_name)}}
              </td>
              <td>
                <a href="#" class="record-edit"><i class="fa fa-pencil"></i> Edit</a> &nbsp;|&nbsp; <a href="#" class="record-delete"><i class="fa fa-trash-o"></i> Delete</a>
              </td>
              @endforeach
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop