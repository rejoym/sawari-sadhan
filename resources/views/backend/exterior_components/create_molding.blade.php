@extends('backend._shared.layouts.master')
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="widget-container my-container products-tile">
      <div class="row widget-alert-wrapper">
        <div class="widget-alert col-md-12">
          @if(Session::get('Success'))
            <div class="alert alert-success my-widget-alert" role="alert">
              <i class="fa fa-check"></i> Success! New Molding has been successfully registered.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          @if($errors->has('input-location') || $errors->has('input-molding'))
            <div class="alert alert-danger my-widget-alert" role="alert">
              <i class="fa fa-exclamation-triangle"></i> Error! Form field is required!
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
        </div>
      </div>
      <div class="widget-content padded">
        <form action="" id="validate-form" method="post" class="product-form">
          {!! csrf_field() !!}
          <fieldset>
            <div class="heading no-padding-lr">
              <i class="fa fa-align-left"></i>Add Molding
            </div>
            <div class="heading no-padding-lr gray">
              <i class="fa fa-file-text-o"></i>Molding specifications
            </div>
            <div class="heading-info">
              <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut nihil voluptas sit est ratione ab.</span>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="molding-type">Molding Type</label>
                  <select class="form-control" name="input-molding-type" id="molding-type">
                    <option value="">Select Molding Type</option>
                    @foreach($data['all_molding_type'] as $molding_type)
                      <option value="{{$molding_type->id}}"{{(old('input-molding-type') == $molding_type->id) ? 'selected' : ''}}>{{ucfirst($molding_type->molding_type)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="location">Location</label>
                  <select class="form-control" name="input-location" id="location">
                    <option value="">Select Location</option>
                    @foreach($data['all_location'] as $locations)
                      <option value="{{$locations->id}}"{{(old('input-location') == $locations->id) ? 'selected' : ''}}>{{ucfirst($locations->location)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary widget-btn"><i class="ion-ios-send"></i>&nbsp;SAVE</button>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <!-- Striped Table -->
  <div class="col-lg-12">
    <div class="widget-container fluid-height clearfix my-container">
      <div class="heading">
        <i class="fa fa-table"></i>All Available Body Part Colors
      </div>
      <div class="widget-content padded clearfix">
        <table class="table table-striped" id="datatable-editable">
          <thead>
            <th>S-N</th>
            <th>MOLDING TYPE</th>
            <th>LOCATION</th>
            <th>ACTION</th>
          </thead>
          <tbody>
            <?php $sn = 0; ?>
            @foreach($data['all_molding'] as $moldings)
            <tr>
              <td>
                {{++$sn}}
              </td>
              <td>
                {{ucfirst($moldings->moldingType->molding_type)}}
              </td>
              <td>
                {{ucfirst($moldings->location->location)}}
              </td>
              <td>
                <a href="#" class="record-edit"><i class="fa fa-pencil"></i> Edit</a> &nbsp;|&nbsp; <a href="#" class="record-delete"><i class="fa fa-trash-o"></i> Delete</a>
              </td>
              @endforeach
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@stop