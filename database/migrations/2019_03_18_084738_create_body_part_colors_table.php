<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyPartColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_part_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('body_part_id')->unsigned();
            $table->integer('color_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('body_part_colors', function (Blueprint $table) {
            $table->foreign('body_part_id')
                  ->references('id')
                  ->on('body_parts')
                  ->onUpdate('cascade');
            $table->foreign('color_id')
                  ->references('id')
                  ->on('colors')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_part_colors');
    }
}
