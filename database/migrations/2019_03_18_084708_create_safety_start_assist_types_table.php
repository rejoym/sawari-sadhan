<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafetyStartAssistTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safety_start_assist_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('safety_id')->unsigned();
            $table->integer('start_assist_type_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('safety_start_assist_types', function (Blueprint $table) {
            $table->foreign('safety_id')
                  ->references('id')
                  ->on('safeties')
                  ->onUpdate('cascade');
            $table->foreign('start_assist_type_id')
                  ->references('id')
                  ->on('start_assist_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safety_start_assist_types');
    }
}
