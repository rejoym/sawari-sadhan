<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirBagLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('air_bag_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('air_bag_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('air_bag_locations', function (Blueprint $table) {
            $table->foreign('air_bag_id')
                  ->references('id')
                  ->on('air_bags')
                  ->onUpdate('cascade');
            $table->foreign('location_id')
                  ->references('id')
                  ->on('locations')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('air_bag_locations');
    }
}
