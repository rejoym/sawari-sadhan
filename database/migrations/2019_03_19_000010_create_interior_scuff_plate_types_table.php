<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteriorScuffPlateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interior_scuff_plate_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interior_id')->unsigned();
            $table->integer('scuff_plate_type_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('interior_scuff_plate_types', function (Blueprint $table) {
            $table->foreign('interior_id')
                  ->references('id')
                  ->on('interiors')
                  ->onUpdate('cascade');
            $table->foreign('scuff_plate_type_id')
                  ->references('id')
                  ->on('scuff_plate_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interior_scuff_plate_types');
    }
}
