<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 50);
            $table->string('password', 50);
            $table->string('first_name', 25);
            $table->string('middle_name', 25)->nullable();
            $table->string('last_name', 25);
            $table->string('username', 25);
            $table->enum('gender', ['male', 'female', 'others']);
            $table->date('date_of_birth');
            $table->string('city', 25);
            $table->string('country', 25);
            $table->tinyInteger('email_verified')->default('0');
            $table->date('registration_date');
            $table->string('verification_code', 8);
            $table->string('phone_number', 20);
            $table->tinyInteger('phone_verified')->default('0');
            $table->string('address', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_users');
    }
}
