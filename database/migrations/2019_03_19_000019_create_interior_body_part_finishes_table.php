<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteriorBodyPartFinishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interior_body_part_finishes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('body_part_finish_id')->unsigned();
            $table->integer('interior_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('interior_body_part_finishes', function (Blueprint $table) {
            $table->foreign('body_part_finish_id')
                  ->references('id')
                  ->on('body_part_finishes')
                  ->onUpdate('cascade');
            $table->foreign('interior_id')
                  ->references('id')
                  ->on('interiors')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interior_body_part_finishes');
    }
}
