<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentWarningIndicatorTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_warning_indicator_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instrument_id')->unsigned();
            $table->integer('warning_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('instrument_warning_indicator_types', function (Blueprint $table) {
            $table->foreign('instrument_id')
                  ->references('id')
                  ->on('instruments')
                  ->onUpdate('cascade');
            $table->foreign('warning_id')
                  ->references('id')
                  ->on('warning_indicator_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_warning_indicator_types');
    }
}
