<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafetyParkingAssistMechanismsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safety_parking_assist_mechanisms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('safety_id')->unsigned();
            $table->integer('parking_assist_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('safety_parking_assist_mechanisms', function (Blueprint $table) {
            $table->foreign('safety_id')
                  ->references('id')
                  ->on('safeties')
                  ->onUpdate('cascade');
            $table->foreign('parking_assist_id')
                  ->references('id')
                  ->on('parking_assist_mechanisms')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safety_parking_assist_mechanisms');
    }
}
