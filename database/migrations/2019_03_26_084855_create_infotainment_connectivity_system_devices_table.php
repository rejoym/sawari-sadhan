<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfotainmentConnectivitySystemDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_connect_sys_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('info_connect_id')->unsigned();
            $table->integer('info_sys_device_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('info_connect_sys_devices', function (Blueprint $table) {
            $table->foreign('info_connect_id')
                  ->references('id')
                  ->on('infotainment_connectivities')
                  ->onUpdate('cascade');
            $table->foreign('info_sys_device_id')
                  ->references('id')
                  ->on('infotainment_system_devices')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_connect_sys_devices');
    }
}
