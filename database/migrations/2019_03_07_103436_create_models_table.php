<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('modal_name', 255);
            $table->integer('brand_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('modals', function (Blueprint $table) {
            $table->foreign('brand_id')
                  ->references('id')
                  ->on('brands')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modals');
        Schema::dropIfExists('brands');
    }
}
