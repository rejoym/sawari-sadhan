<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('variant_name', 255);
            $table->integer('brand_id')->unsigned();
            $table->integer('modal_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('variants', function (Blueprint $table) {
            $table->foreign('brand_id')
                  ->references('id')
                  ->on('brands')
                  ->onUpdate('cascade');
            $table->foreign('modal_id')
                  ->references('id')
                  ->on('modals')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
        Schema::dropIfExists('modals');
        Schema::dropIfExists('brands');
    }
}
