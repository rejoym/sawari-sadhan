<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lamps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned();
            $table->integer('lamp_type_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('lamps', function (Blueprint $table) {
            $table->foreign('location_id')
                  ->references('id')
                  ->on('locations')
                  ->onUpdate('cascade');
            $table->foreign('lamp_type_id')
                  ->references('id')
                  ->on('lamp_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lamps');
    }
}
