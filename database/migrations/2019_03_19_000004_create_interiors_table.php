<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteriorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interiors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->boolean('rear_parcel_tray');
            $table->boolean('rear_seat_center_armrest');
            $table->integer('doorhandle_finish_id')->unsigned();
            $table->integer('steering_wheel_id')->unsigned();
            $table->integer('gear_knob_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('interiors', function (Blueprint $table) {
            $table->foreign('doorhandle_finish_id')
                  ->references('id')
                  ->on('doorhandle_finishes')
                  ->onUpdate('cascade');
            $table->foreign('steering_wheel_id')
                  ->references('id')
                  ->on('steering_wheels')
                  ->onUpdate('cascade');
            $table->foreign('gear_knob_id')
                  ->references('id')
                  ->on('gear_knobs')
                  ->onUpdate('cascade');
            $table->foreign('product_id')
                  ->references('id')
                  ->on('products')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interiors');
    }
}
