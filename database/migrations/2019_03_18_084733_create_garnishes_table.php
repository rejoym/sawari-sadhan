<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGarnishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garnishes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned();
            $table->integer('garnish_type_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('garnishes', function (Blueprint $table) {
            $table->foreign('location_id')
                  ->references('id')
                  ->on('locations')
                  ->onUpdate('cascade');
            $table->foreign('garnish_type_id')
                  ->references('id')
                  ->on('garnish_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('garnishes');
    }
}
