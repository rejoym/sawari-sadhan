<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafetyAirBagLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safety_air_bag_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('air_bag_location_id')->unsigned();
            $table->integer('safety_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('safety_air_bag_locations', function (Blueprint $table) {
            $table->foreign('air_bag_location_id')
                  ->references('id')
                  ->on('air_bag_locations')
                  ->onUpdate('cascade');
            $table->foreign('safety_id')
                  ->references('id')
                  ->on('safeties')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safety_air_bag_locations');
    }
}
