<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComfortKeylessEntryTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comfort_keyless_entry_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comfort_id')->unsigned();
            $table->integer('keyless_entry_type_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('comfort_keyless_entry_types', function (Blueprint $table) {
            $table->foreign('comfort_id')
                  ->references('id')
                  ->on('comforts')
                  ->onUpdate('cascade');
            $table->foreign('keyless_entry_type_id')
                  ->references('id')
                  ->on('keyless_entry_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comfort_keyless_entry_types');
    }
}
