<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_code', 25);
            $table->integer('wheel_id')->unsigned();
            $table->integer('condition_id')->unsigned();
            $table->integer('product_type_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            $table->integer('modal_id')->unsigned();
            $table->integer('variant_id')->unsigned();
            $table->integer('product_release_id')->unsigned();
            $table->integer('fuel_id')->unsigned();
            $table->integer('transmission_id')->unsigned();
            $table->string('milage', 25);
            $table->decimal('price', 8, 2);
            $table->timestamps();
        });
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('wheel_id')
                  ->references('id')
                  ->on('wheels')
                  ->onUpdate('cascade');
            $table->foreign('condition_id')
                  ->references('id')
                  ->on('conditions')
                  ->onUpdate('cascade');
            $table->foreign('product_type_id')
                  ->references('id')
                  ->on('product_types')
                  ->onUpdate('cascade');
            $table->foreign('brand_id')
                  ->references('id')
                  ->on('brands')
                  ->onUpdate('cascade');
            $table->foreign('modal_id')
                  ->references('id')
                  ->on('modals')
                  ->onUpdate('cascade');
            $table->foreign('variant_id')
                  ->references('id')
                  ->on('variants')
                  ->onUpdate('cascade');
            $table->foreign('product_release_id')
                  ->references('id')
                  ->on('product_releases')
                  ->onUpdate('cascade');
            $table->foreign('fuel_id')
                  ->references('id')
                  ->on('fuels')
                  ->onUpdate('cascade');
            $table->foreign('transmission_id')
                  ->references('id')
                  ->on('transmissions')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('brands');
        Schema::dropIfExists('modals');
        Schema::dropIfExists('variants');
    }
}
