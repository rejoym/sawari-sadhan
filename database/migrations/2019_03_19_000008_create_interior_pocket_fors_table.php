<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteriorPocketForsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interior_pocket_fors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interior_id')->unsigned();
            $table->integer('pack_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('interior_pocket_fors', function (Blueprint $table) {
            $table->foreign('interior_id')
                  ->references('id')
                  ->on('interiors')
                  ->onUpdate('cascade');
            $table->foreign('pack_id')
                  ->references('id')
                  ->on('packs')
                  ->onUpdate('cascade');
            $table->foreign('location_id')
                  ->references('id')
                  ->on('locations')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interior_pocket_fors');
    }
}
