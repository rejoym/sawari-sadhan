<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstruMultiInfoDisplayDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instru_multi_info_display_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instrument_id')->unsigned();
            $table->integer('multi_info_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('instru_multi_info_display_devices', function (Blueprint $table) {
            $table->foreign('instrument_id')
                  ->references('id')
                  ->on('instruments')
                  ->onUpdate('cascade');
            $table->foreign('multi_info_id')
                  ->references('id')
                  ->on('multi_information_display_devices')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_multi_information_display_devices');
    }
}
