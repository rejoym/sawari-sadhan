<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComfortWasherWiperTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comfort_washer_wiper_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comfort_id')->unsigned();
            $table->integer('washer_wiper_type_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('comfort_washer_wiper_types', function (Blueprint $table) {
            $table->foreign('comfort_id')
                  ->references('id')
                  ->on('comforts')
                  ->onUpdate('cascade');
            $table->foreign('washer_wiper_type_id')
                  ->references('id')
                  ->on('washer_wiper_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comfort_washer_wiper_types');
    }
}
