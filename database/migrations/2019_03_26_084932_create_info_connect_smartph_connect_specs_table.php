<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoConnectSmartphConnectSpecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_connect_smartph_connect_specs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('info_connect_id')->unsigned();
            $table->integer('smartph_connect_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('info_connect_smartph_connect_specs', function (Blueprint $table) {
            $table->foreign('info_connect_id')
                  ->references('id')
                  ->on('infotainment_connectivities')
                  ->onUpdate('cascade');
            $table->foreign('smartph_connect_id')
                  ->references('id')
                  ->on('smartphone_connectivity_specs')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_connect_smartph_connect_specs');
    }
}
