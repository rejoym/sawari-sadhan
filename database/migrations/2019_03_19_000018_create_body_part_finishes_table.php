<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyPartFinishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_part_finishes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('body_part_id')->unsigned();
            $table->integer('finish_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('body_part_finishes', function (Blueprint $table) {
            $table->foreign('body_part_id')
                  ->references('id')
                  ->on('body_parts')
                  ->onUpdate('cascade');
            $table->foreign('finish_id')
                  ->references('id')
                  ->on('finishes')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_part_finishes');
    }
}
