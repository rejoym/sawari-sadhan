<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExteriorBodyPartFinishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exterior_body_part_finishes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('body_part_finish_id')->unsigned();
            $table->integer('exterior_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('exterior_body_part_finishes', function (Blueprint $table) {
            $table->foreign('body_part_finish_id')
                  ->references('id')
                  ->on('body_part_finishes')
                  ->onUpdate('cascade');
            $table->foreign('exterior_id')
                  ->references('id')
                  ->on('exteriors')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exterior_body_part_finishes');
    }
}
