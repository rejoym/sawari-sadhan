<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafetyBrakingSystemTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safety_braking_system_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('safety_id')->unsigned();
            $table->integer('braking_system_type_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('safety_braking_system_types', function (Blueprint $table) {
            $table->foreign('safety_id')
                  ->references('id')
                  ->on('safeties')
                  ->onUpdate('cascade');
            $table->foreign('braking_system_type_id')
                  ->references('id')
                  ->on('braking_system_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safety_braking_system_types');
    }
}
