<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSteeringWheelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('steering_wheels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wrapping_id')->unsigned();
            $table->integer('coating_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('steering_wheels', function (Blueprint $table) {
            $table->foreign('wrapping_id')
                  ->references('id')
                  ->on('wrappings')
                  ->onUpdate('cascade');
            $table->foreign('coating_id')
                  ->references('id')
                  ->on('coatings')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('steering_wheels');
    }
}
