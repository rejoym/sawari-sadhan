<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExteriorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exteriors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('alloy_wheel_type_id')->unsigned();
            $table->integer('skid_plate_id')->unsigned();
            $table->integer('lamp_id')->unsigned();
            $table->boolean('roof_rails');
            $table->integer('radiator_grill_id')->unsigned();
            $table->integer('molding_id')->unsigned();
            $table->boolean('step_plate');
            $table->integer('cladding_id')->unsigned();
            $table->boolean('clutch_footrest');
            $table->integer('garnish_id')->unsigned();
            $table->integer('doorhandle_finish_id')->unsigned();
            $table->integer('antenna_id')->unsigned();
            $table->boolean('mirror_indicator');
            $table->timestamps();
        });
        Schema::table('exteriors', function (Blueprint $table) {
            $table->foreign('alloy_wheel_type_id')
                  ->references('id')
                  ->on('alloy_wheel_types')
                  ->onUpdate('cascade');
            $table->foreign('product_id')
                  ->references('id')
                  ->on('products')
                  ->onUpdate('cascade');
            $table->foreign('skid_plate_id')
                  ->references('id')
                  ->on('skid_plates')
                  ->onUpdate('cascade');
            $table->foreign('lamp_id')
                  ->references('id')
                  ->on('lamps')
                  ->onUpdate('cascade');
            $table->foreign('radiator_grill_id')
                  ->references('id')
                  ->on('radiator_grills')
                  ->onUpdate('cascade');
            $table->foreign('molding_id')
                  ->references('id')
                  ->on('moldings')
                  ->onUpdate('cascade');
            $table->foreign('cladding_id')
                  ->references('id')
                  ->on('claddings')
                  ->onUpdate('cascade');
            $table->foreign('garnish_id')
                  ->references('id')
                  ->on('garnishes')
                  ->onUpdate('cascade');
            $table->foreign('doorhandle_finish_id')
                  ->references('id')
                  ->on('doorhandle_finishes')
                  ->onUpdate('cascade');
            $table->foreign('antenna_id')
                  ->references('id')
                  ->on('antennas')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exteriors');
    }
}
