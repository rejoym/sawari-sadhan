<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyPartColorExteriorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_part_color_exteriors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('body_part_color_id')->unsigned();
            $table->integer('exterior_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('body_part_color_exteriors', function (Blueprint $table) {
            $table->foreign('body_part_color_id')
                  ->references('id')
                  ->on('body_part_colors')
                  ->onUpdate('cascade');
            $table->foreign('exterior_id')
                  ->references('id')
                  ->on('exteriors')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_part_color_exteriors');
    }
}
