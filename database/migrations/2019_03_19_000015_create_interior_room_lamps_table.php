<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteriorRoomLampsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interior_room_lamps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interior_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('interior_room_lamps', function (Blueprint $table) {
            $table->foreign('interior_id')
                  ->references('id')
                  ->on('interiors')
                  ->onUpdate('cascade');
            $table->foreign('location_id')
                  ->references('id')
                  ->on('locations')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interior_room_lamps');
    }
}
