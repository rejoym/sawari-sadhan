<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafetyBodyStructureTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safety_body_structure_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('safety_id')->unsigned();
            $table->integer('body_structure_type_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('safety_body_structure_types', function (Blueprint $table) {
            $table->foreign('safety_id')
                  ->references('id')
                  ->on('safeties')
                  ->onUpdate('cascade');
            $table->foreign('body_structure_type_id')
                  ->references('id')
                  ->on('body_structure_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safety_body_structure_types');
    }
}
