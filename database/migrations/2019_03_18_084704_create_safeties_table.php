<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSafetiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('safeties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->boolean('electronic_brakeforce_distribution');
            $table->boolean('electronic_stability_control');
            $table->boolean('vehicle_stability_management_control');
            $table->boolean('day_night_rear_view_mirror');
            $table->boolean('child_seat_anchor');
            $table->boolean('immobilizer');
            $table->boolean('front_fog_lamps');
            $table->boolean('rear_defogger_with_timer');
            $table->boolean('automatic_head_lamps');
            $table->integer('central_locking_id')->unsigned();
            $table->boolean('smart_pedal');
            $table->boolean('clutch_lock');
            $table->integer('audio_video_navigation_id')->unsigned();
            $table->boolean('headlight_escort_function');
            $table->integer('seat_belt_pretensioners');
            $table->boolean('isofix');
            $table->boolean('dual_horn');
            $table->timestamps();
        });
        Schema::table('safeties', function (Blueprint $table) {
            $table->foreign('central_locking_id')
                  ->references('id')
                  ->on('central_lockings')
                  ->onUpdate('cascade');
            $table->foreign('audio_video_navigation_id')
                  ->references('id')
                  ->on('audio_video_navigations')
                  ->onUpdate('cascade');
            $table->foreign('product_id')
                  ->references('id')
                  ->on('products')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('safeties');
    }
}
