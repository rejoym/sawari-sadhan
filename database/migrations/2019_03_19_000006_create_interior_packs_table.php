<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteriorPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interior_packs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interior_id')->unsigned();
            $table->integer('pack_id')->unsigned();
            $table->integer('body_part_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('interior_packs', function (Blueprint $table) {
            $table->foreign('interior_id')
                  ->references('id')
                  ->on('interiors')
                  ->onUpdate('cascade');
            $table->foreign('pack_id')
                  ->references('id')
                  ->on('packs')
                  ->onUpdate('cascade');
            $table->foreign('body_part_id')
                  ->references('id')
                  ->on('body_parts')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interior_packs');
    }
}
