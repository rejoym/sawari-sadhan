<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteriorIlluminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interior_illuminations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('illumination_id')->unsigned();
            $table->integer('interior_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('interior_illuminations', function (Blueprint $table) {
            $table->foreign('illumination_id')
                  ->references('id')
                  ->on('illuminations')
                  ->onUpdate('cascade');
            $table->foreign('interior_id')
                  ->references('id')
                  ->on('interiors')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interior_illuminations');
    }
}
