<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seatings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->boolean('driver_seat_height_adjustable');
            $table->integer('headrest_adjustable_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('seatings', function (Blueprint $table) {
            $table->foreign('headrest_adjustable_id')
                  ->references('id')
                  ->on('headrest_adjustables')
                  ->onUpdate('cascade');
            $table->foreign('product_id')
                  ->references('id')
                  ->on('products')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seatings');
    }
}
