<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstrumentAdvancedSupervisionClustersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instrument_advanced_supervision_clusters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('instrument_id')->unsigned();
            $table->integer('advanced_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('instrument_advanced_supervision_clusters', function (Blueprint $table) {
            $table->foreign('instrument_id')
                  ->references('id')
                  ->on('instruments')
                  ->onUpdate('cascade');
            $table->foreign('advanced_id')
                  ->references('id')
                  ->on('advanced_supervision_clusters')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instrument_advanced_supervision_clusters');
    }
}
