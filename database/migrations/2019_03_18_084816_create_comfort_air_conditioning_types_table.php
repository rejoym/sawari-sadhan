<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComfortAirConditioningTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comfort_air_conditioning_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comfort_id')->unsigned();
            $table->integer('air_conditioning_type_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('comfort_air_conditioning_types', function (Blueprint $table) {
            $table->foreign('comfort_id')
                  ->references('id')
                  ->on('comforts')
                  ->onUpdate('cascade');
            $table->foreign('air_conditioning_type_id')
                  ->references('id')
                  ->on('air_conditioning_types')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comfort_air_conditioning_types');
    }
}
