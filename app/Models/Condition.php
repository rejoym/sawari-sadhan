<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['condition'];

    /**
     * one to many relation between product and condition
     * @return [type] [description]
     */
    public function products()
    {
      return $this->hasMany('App\Models\Product');
    }
}
