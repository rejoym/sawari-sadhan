<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parking_assist_mechanism extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['parking_assist_mechanism'];

    /**
     * [safetyParkingAssistMechanism description]
     * @return [type] [description]
     */
    public function safetyParkingAssistMechanism()
    {
      return $this->hasMany('App\Models\Safety_parking_assist_mechanism');
    }
}
