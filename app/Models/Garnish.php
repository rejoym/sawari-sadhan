<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Garnish extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['location_id', 'garnish_type_id'];

    /**
     * function to establish relation between location and garnish
     * @return [type] [description]
     */
    public function location()
    {
      return $this->belongsTo('App\Models\Location', 'location_id', 'id');
    }

    /**
     * function to establish relation between garnish and garnish type
     * @return [type] [description]
     */
    public function garnishType()
    {
      return $this->belongsTo('App\Models\Garnish_type', 'garnish_type_id', 'id');
    }

    /**
     * function to establish relation between exterior and garnish
     * @return [type] [description]
     */
    public function exteriors()
    {
      return $this->hasMany('App\Models\Exterior');
    }
}
