<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Safety_body_structure_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['body_structure_id', 'body_structure_type_id'];

    /**
     * [bodyStructure description]
     * @return [type] [description]
     */
    public function bodyStructure()
    {
      return $this->belongsTo('App\Models\Body_structure', 'body_structure_id', 'id');
    }

    /**
     * [bodyStructureType description]
     * @return [type] [description]
     */
    public function bodyStructureType()
    {
      return $this->belongsTo('App\Models\Body_structure_type', 'body_structure_type_id', 'id');
    }
}
