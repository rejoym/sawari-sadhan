<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comfort_outside_mirror_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['comfort_id', 'outside_mirror_type_id'];

    /**
     * this model belongsto Comfort
     * @return [type] [description]
     */
    public function comfort()
    {
      return $this->belongsTo('App\Models\Comfort', 'comfort_id', 'id');
    }

    /**
     * this model also belongs to outside mirror type
     * @return [type] [description]
     */
    public function outsideMirrorType()
    {
      return $this->belongsTo('App\Models\Outside_mirror_type', 'outside_mirror_type_id', 'id');
    }
}
