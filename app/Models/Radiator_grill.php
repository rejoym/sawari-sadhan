<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Radiator_grill extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['radiator_grill'];

    /**
     * [exteriors description]
     * @return [type] [description]
     */
    public function exteriors()
    {
      return $this->hasMany('App\Models\Exterior');
    }
}
