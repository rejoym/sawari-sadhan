<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keyless_entry_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['keyless_entry_type'];

    /**
     * this model has many comfortkeylessentrytypes
     * @return [type] [description]
     */
    public function comfortKeylessEntryTypes()
    {
      return $this->hasMany('App\Models\Comfort_keyless_entry_type');
    }

}
