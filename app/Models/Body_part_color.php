<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Body_part_color extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['body_part_id', 'color_id'];

    /**
     * function to establish relation between body part and body part color
     * @return [type] [description]
     */
    public function bodyPart()
    {
      return $this->belongsTo('App\Models\Body_part', 'body_part_id', 'id');
    }


    /**
     * function to establish color and body part color
     * @return [type] [description]
     */
    public function color()
    {
      return $this->belongsTo('App\Models\Color', 'color_id', 'id');
    }


}
