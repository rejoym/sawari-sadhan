<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Molding_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['molding_type'];


    /**
     * [Molding description]
     * @return array
     */
    public function Molding()
    {
      return $this->hasMany('App\Models\Molding');
    }
}
