<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['product_code', 'wheel_id', 'modal_id', 'brand_id', 'product_release_id', 'variant_id', 'product_type_id', 'fuel_id', 'transmission_id', 'milage', 'price', 'condition_id'];

    /**
     * one to many relation between model and product
     * @return [type] [description]
     */
    public function modal()
    {
      return $this->belongsTo('App\Models\Modal', 'modal_id', 'id');
    }

    /**
     * one to many relation between model and brand
     * @return [type] [description]
     */
    public function brand()
    {
      return $this->belongsTo('App\Models\Brand', 'brand_id', 'id');
    }

    /**
     * one to many relation between product and variant
     * @return [type] [description]
     */
    public function variant()
    {
      return $this->belongsTo('App\Models\Variant', 'variant_id', 'id');
    }

    /**
     * [productType description]
     * @return [type] [description]
     */
    public function productType()
    {
        return $this->belongsTo('App\Models\Product_type', 'product_type_id', 'id');
    }
    /**
     * [productReleaseDate description]
     * @return [type] [description]
     */
    public function productReleaseDate()
    {
        return $this->belongsTo('App\Models\Product_release', 'product_release_id', 'id');
    }

    /**
     * [productColor description]
     * @return [type] [description]
     */
    public function productColor()
    {
        return $this->hasMany('App\Models\Product_color');
    }
    /**
     * [wheels description]
     * @return [type] [description]
     */
    public function wheel()
    {
        return $this->belongsTo('App\Models\Wheel', 'wheel_id', 'id');
    }

    /**
     * [condition description]
     * @return [type] [description]
     */
    public function condition()
    {
        return $this->belongsTo('App\Models\Condition', 'condition_id', 'id');
    }

    /**
     * [safeties description]
     * @return [type] [description]
     */
    public function safeties()
    {
        return $this->hasMany('App\Models\Safeties');
    }

    /**
     * [exteriors description]
     * @return [type] [description]
     */
    public function exteriors()
    {
        return $this->hasMany('App\MOdels\Exterior');
    }

    /**
     * [interiors description]
     * @return [type] [description]
     */
    public function interiors()
    {
        return $this->hasMany('App\Models\Interiors');
    }

    /**
     * [fuel description]
     * @return [type] [description]
     */
    public function fuel()
    {
        return $this->belongsTo('App\Models\Fuel', 'fuel_id', 'id');
    }

    /**
     * [transmission description]
     * @return [type] [description]
     */
    public function transmission()
    {
        return $this->belongsTo('App\Models\Transmission', 'transmission_id', 'id');
    }
}
