<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interior extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['rear_parcel_tray', 'rear_seat_center_armrest', 'doorhandle_finish_id', 'steering_wheel_id', 'gear_knob_id'];

    /**
     * [doorhandleFinish description]
     * @return [type] [description]
     */
    public function doorhandleFinish()
    {
      return $this->belongsTo('App\Models\Doorhandle_finish', 'doorhandle_finish_id', 'id');
    }

    /**
     * [steeringWheel description]
     * @return [type] [description]
     */
    public function steeringWheel()
    {
      return $this->belongsTo('App\Models\Steering_wheel', 'steering_wheel_id', 'id');
    }

    /**
     * [gearKnob description]
     * @return [type] [description]
     */
    public function gearKnob()
    {
      return $this->belongsTo('App\Models\Gear_knob', 'gear_knob_id', 'id');
    }
}
