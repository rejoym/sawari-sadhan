<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alloy_wheel_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['alloy_wheel_type'];

    /**
     * this model hasmany exterior
     * @return [type] [description]
     */
    public function exteriors()
    {
      return $this->hasMany('App\Models\Exterior');
    }
}
