<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comfort_air_conditioning_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['comfort_id', 'air_conditioning_type_id'];

    /**
     * one to many relation between comfort and comfort_air_conditioning_type
     * @return [type] [description]
     */
    public function comfort()
    {
      return $this->belongsTo('App\Models\Comfort', 'comfort_id', 'id');
    }

    /**
     * one to many relation between comfort and air conditioning type
     * @return [type] [description]
     */
    public function airConditioningType()
    {
      return $this->belongsTo('App\Models\Air_conditioning_type', 'air_conditioning_type', 'id');
    }

}
