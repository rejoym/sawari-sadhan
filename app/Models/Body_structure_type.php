<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Body_structure_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['body_structure_type'];

    /**
     * function to establish relation between safety and body structure type
     * @return [type] [description]
     */
    public function safety()
    {
      return $this->hasMany('App\Models\Safety');
    }


    /**
     * function to establish relation between body structure type and safety_body_structure_type
     * @return [type] [description]
     */
    public function bodyStructureSafety()
    {
      return $this->hasMany('App\Models\Safety_body_structure_type');
    }
}
