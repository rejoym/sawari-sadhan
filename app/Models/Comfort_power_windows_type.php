<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comfort_power_windows_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['comfort_id', 'power_windows_type_id'];

    /**
     * this model belongs to comfort
     * @return [type] [description]
     */
    public function comfort()
    {
      return $this->belongsTo('App\Models\Comfort', 'comfort_id', 'id');
    }

    /**
     * this model also belongs to power windows type table
     * @return [type] [description]
     */
    public function powerWindowsType()
    {
      return $this->belongsTo('App\Models\Power_windows_type', 'power_windows_type_id', 'id');
    }
}
