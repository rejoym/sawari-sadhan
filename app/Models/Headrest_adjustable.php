<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Headrest_adjustable extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['front_seat_headrest', 'rear_seat_headrest'];

    /**
     * [seatings description]
     * @return [type] [description]
     */
    public function seatings()
    {
      return $this->hasMany('App\Models\Seating');
    }
}
