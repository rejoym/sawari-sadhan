<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer_user extends Model
{

  protected $table = 'customer_users';
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['email', 'password', 'first_name', 'middle_name', 'last_name', 'username', 'gender', 'date_of_birth', 'city', 'country', 'email_verified', 'registration_date', 'verification_code', 'phone_number', 'phone_verified', 'address'];

    /**
     * function to establish one to many relation berween Customer_user and verify_user
     * @return [type] [description]
     */
    public function verifyUsers()
    {
      return $this->hasOne('App\Models\VerifyUserByEmail');
    }
}
