<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Multi_information_display_device extends Model
{
    /**
     * [$fillable description]
     * @var array
     */
    protected $fillable = ['multi_information_display_device'];

    /**
     * [instrumentMultiInformationDisplayDevice description]
     * @return [type] [description]
     */
    public function instrumentMultiInformationDisplayDevice()
    {
      return $this->hasMany('App\Models\Instrument_multi_information_display_device');
    }
}
