<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instrument_multi_information_display_device extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['instrument_id', 'multi_information_display_device_id'];

    /**
     * [instrument description]
     * @return [type] [description]
     */
    public function instrument()
    {
      return $this->belongsTo('A\Instrument', 'instrument_id', 'id');
    }

    /**
     * [multiInformationDisplayDevice description]
     * @return [type] [description]
     */
    public function multiInformationDisplayDevice()
    {
      return $this->hasMany('App\Models\Multi_information_display_device');
    }
}
