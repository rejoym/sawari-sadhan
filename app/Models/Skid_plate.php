<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skid_plate extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['front_skid_plate', 'back_skid_plate'];

    /**
     * [exterior description]
     * @return [type] [description]
     */
    public function exterior()
    {
      return $this->hasMany('App\Models\Exterior');
    }
}
