<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Central_locking extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['door', 'tailgate'];

    /**
     * function to establish relation between Central locking and safety
     * @return [type] [description]
     */
    public function safety()
    {
      return $this->hasMany('App\Models\Safety');
    }
}
