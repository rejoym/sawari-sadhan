<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comfort_washer_wiper_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['comfort_id', 'washer_wiper_type_id'];

    /**
     * this model belongs to comfort
     * @return [type] [description]
     */
    public function comfort()
    {
      return $this->belongsTo('App\Models\Comfort', 'comfort_id', 'id');
    }

    /**
     * this model belongs to washer wiper type also
     * @return [type] [description]
     */
    public function washerWiperType()
    {
      return $this->belongsTo('App\Models\Washer_wiper_type', 'washer_wiper_type_id', 'id');
    }
}
