<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comfort_keyless_entry_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['comfort_id', 'keyless_entry_type_id'];

    /**
     * one to many relation between comfort and keyless_entry_type
     * @return [array] [description]
     */
    public function comfort()
    {
      return $this->belongsTo('App\Models\Comfort', 'comfort_id', 'id');
    }

    /**
     * Comfort_keyless_entry_type belongs to keyless entry type
     * @return [type] [description]
     */
    public function keylessEntryType()
    {
      return $this->belongsTo('App\Models\Keyless_entry_type', 'keyless_entry_type_id', 'id');
    }
}
