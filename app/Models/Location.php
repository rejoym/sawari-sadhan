<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['location'];

    /**
     * [lamps description]
     * @return [type] [description]
     */
    public function lamps()
    {
      return $this->hasMany('App\Models\Lamp');
    }

    /**
     * [garnishes description]
     * @return [type] [description]
     */
    public function garnishes()
    {
        return $this->hasMany('App\Models\Garnish');
    }

    /**
     * [molding description]
     * @return [type] [description]
     */
    public function molding()
    {
      return $this->hasMany('App\Models\Molding');
    }


    public function cladding()
    {
      return $this->hasMany('App\Models\Cladding');
    }



}
