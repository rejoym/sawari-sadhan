<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Safety_start_assist_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['safety_id', 'start_assist_type_id'];

    /**
     * [safety description]
     * @return [type] [description]
     */
    public function safety()
    {
      return $this->belongsTo('App\Models\Safety', 'safety_id', 'id');
    }

    /**
     * [startAssistType description]
     * @return [type] [description]
     */
    public function startAssistType()
    {
      return $this->belongsTo('App\Models\Start_assist_type', 'start_assist_type_id', 'id');
    }
}
