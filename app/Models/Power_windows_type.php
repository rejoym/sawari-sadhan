<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Power_windows_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['power_windows_type'];

    /**
     * this model has many comfort power windows type
     * @return [type] [description]
     */
    public function comfortPowerWindowsType()
    {
      return $this->hasMany('App\Models\Comfort_power_windows_type');
    }
}
