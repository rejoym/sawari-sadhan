<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doorhandle_finish extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['doorhandle_finish'];

    /**
     * function to establish relation between doorhandle_finish and exterior
     * @return [type] [description]
     */
    public function exteriors()
    {
      return $this->hasMany('App\Models\Exterior');
    }

    /**
     * function to establish relation between interior and door handle finish
     * @return [type] [description]
     */
    public function interiors()
    {
      return $this->hasMany('App\Models\Interior');
    }
}
