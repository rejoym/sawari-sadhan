<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Outside_mirror_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['outside_mirror_type'];

    /**
     * this model has many comfortOutsideMirrorTypes
     * @return [type] [description]
     */
    public function comfortOutsideMirrorTypes()
    {
      return $this->hasMany('App\Models\Comfort_outside_mirror_type');
    }
}
