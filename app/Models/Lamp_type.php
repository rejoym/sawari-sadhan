<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lamp_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['lamp_type'];


    public function lamps()
    {
      return $this->hasMany('App\Models\Lamp');
    }
}
