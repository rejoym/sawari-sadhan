<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warning_indicator_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['warning_indicator_type'];

    /**
     * [instrumentWarningIndicatorTypes description]
     * @return [type] [description]
     */
    public function instrumentWarningIndicatorTypes()
    {
      return $this->hasMany('App\Models\Instrument_warning_indicator_type');
    }
}
