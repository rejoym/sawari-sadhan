<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fuel extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['fuel_type'];

    /**
     * [products description]
     * @return [type] [description]
     */
    public function products()
    {
      return $this->hasMany('App\Models\Product');
    }
}
