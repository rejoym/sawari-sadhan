<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exterior extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['alloy_wheel_type_id', 'skid_plate_id', 'lamps_id', 'roof_rails', 'radiator_grill_id', 'body_part_color_exterior_id', 'molding_id', 'step_plate', 'cladding_id', 'clutch_footrest', 'garnish_id', 'doorhandle_finish_id', 'antenna_id', 'mirror_indicator'];

    /**
     * [alloyWheel description]
     * @return [type] [description]
     */
    public function alloyWheel()
    {
      return $this->belongsTo('App\Models\Alloy_wheel_type', 'alloy_wheel_type_id', 'id');
    }

    /**
     * function to establish relation between skid plate and exterior
     * @return [type] [description]
     */
    public function skidPlate()
    {
      return $this->belongsTo('App\Models\Skid_plate', 'skid_plate_id', 'id');
    }

    /**
     * function to establish relation between lamp and exterior
     * @return [type] [description]
     */
    public function lamp()
    {
      return $this->belongsTo('App\Models\Lamps', 'lamps_id', 'id');
    }

    /**
     * function to establish relation between radiator grill and exterior
     * @return [type] [description]
     */
    public function radidatorGrill()
    {
      return $this->belongsTo('App\Models\Radiator_grill', 'radiator_grill_id', 'id');
    }

    public function bodyPartColorExterior()
    {
        return $this->hasMany('App\Models\Body_part_color_exterior');
    }
    /**
     * function to establish relation between molding and exterior
     * @return [type] [description]
     */
    public function molding()
    {
      return $this->belongsTo('App\Models\Molding', 'molding_id', 'id');
    }

    /**
     * function to establish relation between cladding and exterior
     * @return [type] [description]
     */
    public function cladding()
    {
      return $this->belongsTo('App\Models\Cladding', 'cladding_id', 'id');
    }

    /**
     * function to establish relation between garnish and exterior
     * @return [type] [description]
     */
    public function garnish()
    {
      return $this->belongsTo('App\Models\Garnish', 'garnish_id', 'id');
    }

    /**
     * function to establish relation between door handle finish and exterior
     * @return [type] [description]
     */
    public function doorHandleFinish()
    {
      return $this->belongsTo('App\Models\Doorhandle_finish', 'doorhandle_finish_id', 'id');
    }

    /**
     * function to establish relation between antenna and exterior
     * @return [type] [description]
     */
    public function antenna()
    {
      return $this->belongsTo('App\Models\Antenna', 'antenna_id', 'id');
    }

    /**
     * function to establish relation between product and exterior
     * @return [type] [description]
     */
    public function products()
    {
      return $this->hasMany('App\Models\Product');
    }
}
