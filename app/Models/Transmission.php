<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transmission extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['transmission_type'];

    /**
     * [products description]
     * @return [type] [description]
     */
    public function products()
    {
      return $this->hasMany('App\Models\Product');
    }
}
