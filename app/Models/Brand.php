<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['brand_name'];

    /**
     * one to many relation between model and brand
     * @return [object] [description]
     */
    public function modals()
    {
      return $this->hasMany('App\Models\Modal');
    }

    /**
     * one to many relation between Brand and Variants
     * @return [object] [description]
     */
    public function variants()
    {
      return $this->hasMany('App\Models\Variant');
    }

    /**
     * one to many relation between brand and products
     * @return [object] [description]
     */
    public function products()
    {
      return $this->hasMany('App\Models\Product');
    }
}
