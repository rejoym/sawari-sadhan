<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Antenna extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['antenna'];

    /**
     * function to establish relation between exterior and antenna
     * @return [type] [description]
     */
    public function exteriors()
    {
      return $this->hasMany('App\Models\Exterior');
    }
}
