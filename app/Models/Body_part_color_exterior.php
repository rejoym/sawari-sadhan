<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Body_part_color_exterior extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['body_part_color_id', 'exterior_id'];

    /**
     * function to establish relation between body part color and body part color exterior
     * @return [type] [description]
     */
    public function bodyPartColor()
    {
      return $this->belongsTo('App\Models\Body_part_color', 'body_part_color_id', 'id');
    }

    /**
     * function to establish relation between exterior and body part color exterior
     * @return [type] [description]
     */
    public function exterior()
    {
      return $this->belongsTo('App\Models\Exterior', 'exterior_id', 'id');
    }

}
