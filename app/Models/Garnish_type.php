<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Garnish_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['garnish_type'];

    /**
     * function to establish relation between garnish and garnish_type
     * @return [type] [description]
     */
    public function garnish()
    {
      return $this->hasMany('App\Models\Garnish');
    }
}
