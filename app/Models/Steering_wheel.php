<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Steering_wheel extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['wrapping_id', 'coating_id'];

    /**
     * [wrapping description]
     * @return [type] [description]
     */
    public function wrapping()
    {
      return $this->belongsTo('App\Models\Wrapping', 'wrapping_id', 'id');
    }

    /**
     * [coating description]
     * @return [type] [description]
     */
    public function coating()
    {
      return $this->belongsTo('App\Models\Coating', 'coating_id', 'id');
    }

    /**
     * [interiors description]
     * @return [type] [description]
     */
    public function interiors()
    {
      return $this->hasMany('App\Models\Interior');
    }
}
