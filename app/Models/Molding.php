<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Molding extends Model
{
    /**
     * arrays that are mass assognable
     * @var [type]
     */
    protected $fillable = ['location_id', 'molding_type_id'];

    /**
     * [location description]
     * @return [type] [description]
     */
    public function location()
    {
      return $this->belongsTo('App\Models\Location', 'location_id', 'id');
    }

    /**
     * [moldingType description]
     * @return [type] [description]
     */
    public function moldingType()
    {
      return $this->belongsTo('App\Models\Molding_type', 'molding_id', 'id');
    }
}
