<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Safety_braking_system_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['safety_id', 'braking_system_type_id'];

    /**
     * [safety description]
     * @return [type] [description]
     */
    public function safety()
    {
      return $this->belongsTo('App\Models\Safety', 'safety_id', 'id');
    }

    /**
     * [brakingSystemType description]
     * @return [type] [description]
     */
    public function brakingSystemType()
    {
      return $this->belongsTo('App\Models\Braking_system_type', 'braking_system_type_id', 'id');
    }
}
