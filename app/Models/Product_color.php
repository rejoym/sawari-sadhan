<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_color extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['product_id', 'color_id'];

    /**
     * [product description]
     * @return [type] [description]
     */
    public function product()
    {
      return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

    /**
     * [color description]
     * @return [type] [description]
     */
    public function color()
    {
      return $this->belongsTo('App\Models\Color', 'color_id', 'id');
    }
}
