<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wrapping extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['wrapping'];

    /**
     * [steeringWheels description]
     * @return [type] [description]
     */
    public function steeringWheels()
    {
      return $this->hasMany('App\Models\Steering_wheel');
    }

    /**
     * [gearKnobs description]
     * @return [type] [description]
     */
    public function gearKnobs()
    {
      return $this->hasMany('App\Models\Gear_knob');
    }
}
