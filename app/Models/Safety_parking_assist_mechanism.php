<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Safety_parking_assist_mechanism extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['safety_id', 'parking_assist_mechanism_id'];

    /**
     * [safety description]
     * @return [type] [description]
     */
    public function safety()
    {
      return $this->belongsTo('App\Models\Safety', 'safety_id', 'id');
    }

    /**
     * [parkingAssistMechanism description]
     * @return [type] [description]
     */
    public function parkingAssistMechanism()
    {
      return $this->belongsTo('App\Models\Parking_assist_mechanism', 'parking_assist_mechanism_id', 'id');
    }
}
