<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instrument extends Model
{

    /**
     * function to establish relation between instrument and instrument warning indicator type
     * @return [type] [description]
     */
    public function instrumentWarningIndicatorType()
    {
      return $this->hasMany('App\Models\Instrument_warning_indicator_type');
    }

    /**
     * function to establish relation between instrument and instrument advanced supervision cluster
     * @return [type] [description]
     */
    public function instrumentAdvancedSupervisionCluster()
    {
      return $this->hasMany('App\Models\Instrument_advanced_supervision_cluster');
    }

    /**
     * function to establish relation between instrument and multi information display device
     * @return [type] [description]
     */
    public function instrumentMultiInformationDisplayDevice()
    {
      return $this->hasMany('App\Models\Instrument_multi_information_display_device');
    }
}
