<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Air_conditioning_type extends Model
{
  /**
   * arrays that are mass assignable
   * @var array
   */
    protected $fillable = ['air_conditioning_type'];

    /**
     * this model has many air conditioning types
     * @return [type] [description]
     */
    public function comfortAirConditioningTypes()
    {
      return $this->hasMany('App\Models\Comfort_air_conditioning_type');
    }
}
