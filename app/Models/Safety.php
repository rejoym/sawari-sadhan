<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Safety extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['product_id', 'electronic_brakeforce_distribution', 'electronic_stability_control', 'vehicle_stability_management_control',  'day-night_rear_view_mirror', 'child_seat_anchor', 'immobilizer', 'front_fog_lamps', 'rear_defogger_with_timer', 'automatic_head_lamps', 'central_locking_id', 'smart_pedal', 'clutch_lock', 'audio_video_navigation_id', 'headlight_escort_function', 'seat_belt_pretensioners', 'isofix', 'dual_horn'];


    /**
     * [safetyBrakingSystem description]
     * @return [type] [description]
     */
    public function safetyBrakingSystem()
    {
        return $this->hasMany('App\Models\Safety_braking_system');
    }
    /**
     * [safetyParkingAssistMechanism description]
     * @return [type] [description]
     */
    public function safetyParkingAssistMechanism()
    {
      return $this->hasMany('App\Models\Safety_parking_assist_mechanism');
    }

    /**
     * [startAssistType description]
     * @return [type] [description]
     */
    public function safetyStartAssistType()
    {
      return $this->hasMany('App\Models\Safety_start_assist_type');
    }

    /**
     * [bodyStructureType description]
     * @return [type] [description]
     */
    public function safetyBodyStructureType()
    {
      return $this->hasMany('App\Models\Safety_body_structure_type');
    }

    /**
     * [centralLocking description]
     * @return [type] [description]
     */
    public function centralLocking()
    {
      return $this->belongsTo('App\Models\Central_locking', 'central_locking_id', 'id');
    }

    /**
     * [audioVideoNavigation description]
     * @return [type] [description]
     */
    public function audioVideoNavigation()
    {
      return $this->belongsTo('App\Models\Audio_video_navigation', 'audio_video_navigation_id', 'id');
    }

    /**
     * [product description]
     * @return [type] [description]
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

}
