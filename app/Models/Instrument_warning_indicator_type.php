<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instrument_warning_indicator_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['instrument_id', 'warning_indicator_type_id'];

    /**
     * [instrument description]
     * @return [type] [description]
     */
    public function instrument()
    {
      return $this->belongsTo('App\Models\Instrument', 'instrument_id', 'id');
    }

    /**
     * [warningIndicatorType description]
     * @return [type] [description]
     */
    public function warningIndicatorType()
    {
      return $this->hasMany('App\Models\Warning_indicator_type');
    }
}
