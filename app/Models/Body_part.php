<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Body_part extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['body_part'];

    /**
     * function to establish relation between body part colors and body part
     * @return [type] [description]
     */
    public function bodyPartColors()
    {
      return $this->hasMany('App\Models\Body_part_color');
    }
}
