<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comfort extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['product_id'];

    /**
     * one to many relation between comfort and keyless entry types
     * @return [array] [description]
     */
    public function keylessEntries()
    {
      return $this->hasMany('App\Models\Comfort_keyless_entry_type');
    }
    /**
     * one to many relation between comfort and air_conditioning_type
     * @return [array] [description]
     */
    public function airConditionings()
    {
      return $this->hasMany('App\Models\Comfort_air_conditioning_type');
    }

    /**
     * one to many relation between comfort and Outside_mirror_type
     * @return [array] [description]
     */
    public function outsideMirrors()
    {
      return $this->hasMany('App\Models\Comfort_outside_mirror_type');
    }

    /**
     * one to many relation between comfort and Comfort_power_windows_type
     * @return [array] [description]
     */
    public function powerWindows()
    {
      return $this->hasMany('App\Models\Comfort_power_windows_type');
    }

    /**
     * one to many relation between comfort and Comfort_washer_wiper_type
     * @return [array] [description]
     */
    public function washerWipers()
    {
      return $this->hasMany('App\Models\Comfort_washer_wiper_type');
    }

    /**
     * [product description]
     * @return [type] [description]
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }

}
