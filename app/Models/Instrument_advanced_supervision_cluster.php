<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instrument_advanced_supervision_cluster extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['instrument_id', 'advanced_supervision_cluster_id'];

    /**
     * function to establish relation between instrument and instrument advanced cluster
     * @return [type] [description]
     */
    public function instrument()
    {
      return $this->belongsTo('App\Models\Instrument');
    }

    /**
     * function to establish relation between instrument advanced supervision cluster and advanced supervision cluster
     * @return [type] [description]
     */
    public function advancedSupervisionCluster()
    {
      return $this->hasMany('App\Models\Advanced_supervision_cluster');
    }
}
