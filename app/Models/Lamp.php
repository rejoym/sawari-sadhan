<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lamp extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['location_id', 'lamp_type_id'];

    /**
     * function to establish relation between location and lamps
     * @return [type] [description]
     */
    public function location()
    {
      return $this->belongsTo('App\Models\Location', 'location_id', 'id');
    }

    /**
     * [lampType description]
     * @return [type] [description]
     */
    public function lampType()
    {
      return $this->belongsTo('App\Models\Lamp_type', 'lamp_type_id', 'id');
    }
}
