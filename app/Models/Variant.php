<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $fillable = ['variant_name', 'brand_id', 'modal_id'];

    /**
     * one to many relation between Variant and brand
     * @return [type] [description]
     */
    public function brands()
    {
      return $this->belongsTo('App\Models\Brand', 'brand_id', 'id');
    }

    /**
     * one to many relation between model and variant
     * @return [type] [description]
     */
    public function modals()
    {
      return $this->belongsTo('App\Models\Modal', 'modal_id', 'id');
    }

    /**
     * one to many relation between product and variant
     * @return [type] [description]
     */
    public function products()
    {
      return $this->hasMany('App\Models\Product');
    }
}
