<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Washer_wiper_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['washer_wiper_type'];

    /**
     * this model has many washer wiper type
     * @return [type] [description]
     */
    public function comfortWasherWiperType()
    {
      return $this->hasMany('App\Models\Comfort_washer_wiper_type');
    }
}
