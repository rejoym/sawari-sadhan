<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cladding extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['location_id', 'cladding_type_id'];

    /**
     * function to establish relation between location and cladding
     * @return [type] [description]
     */
    public function location()
    {
      return $this->belongsTo('App\Models\Location', 'location_id', 'id');
    }

    /**
     * function to establish relation between cladding and cladding_type
     * @return [type] [description]
     */
    public function claddingType()
    {
      return $this->belongsTo('App\Models\Cladding_type', 'cladding_type_id', 'id');
    }
}
