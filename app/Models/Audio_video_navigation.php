<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Audio_video_navigation extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['avn', 'avn_rear_view_camera_display'];

    /**
     * function to establish relation between safety and audio_video_navigation
     * @return [type] [description]
     */
    public function safety()
    {
      return $this->hasMany('App\Models\Safety');
    }
}
