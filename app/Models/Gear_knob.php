<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gear_knob extends Model
{
    /**
     * arrays that are mass assignable
     * @var [type]
     */
    protected $fillable = ['wrapping_id', 'coating_id'];

    /**
     * [wrapping description]
     * @return [type] [description]
     */
    public function wrapping()
    {
      return $this->belongsTo('App\Models\Wrapping', 'wrapping_id', 'id');
    }

    /**
     * [wrapping description]
     * @return [type] [description]
     */
    public function coating()
    {
      return $this->belongsTo('App\Models\Coating', 'coating_id', 'id');
    }
}
