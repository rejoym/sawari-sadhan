<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seating extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['driver_seat_height_adjustable', 'headrest_adjustable_id'];

    /**
     * [headrestAdjustable description]
     * @return [type] [description]
     */
    public function headrestAdjustable()
    {
      return $this->belongsTo('App\Models\Headrest_adjustable', 'headrest_adjustable_id', 'id');
    }

    /**
     * [products description]
     * @return [type] [description]
     */
    public function products()
    {
      return $this->hasMany('App\Models\Product');
    }
}
