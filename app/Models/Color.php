<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['color_name'];

    /**
     * function to establish relation between body part color and color
     * @return [type] [description]
     */
    public function bodyPartColor()
    {
      return $this->hasMany('App\Models\Body_part_color');
    }
}
