<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Braking_system_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['braking_system_type'];

    /**
     * function to establish relation between safety braking system type and braking system type
     * @return [type] [description]
     */
    public function safetyBrakingSystemType()
    {
      return $this->hasMany('App\Models\Safety_braking_system_type');
    }
}
