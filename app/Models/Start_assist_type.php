<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Start_assist_type extends Model
{
    /**
     * arrays that are mass assignable
     * @var array
     */
    protected $fillable = ['start_assist_type'];

    /**
     * [startAssistTypeSafety description]
     * @return [type] [description]
     */
    public function safetyStartAssistType()
    {
      return $this->hasMany('App\Models\Start_assist_type_safety');
    }




}
