<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modal extends Model
{
    protected $fillable = ['modal_name', 'brand_id'];

    /**
     * one to many relation between Model and Brands
     * @return [type] [description]
     */
    public function brands()
    {
      return $this->belongsTo('App\Models\Brand', 'brand_id', 'id');
    }

    /**
     * one to many relation between Model and variant
     * @return [type] [description]
     */
    public function variants()
    {
      return $this->hasMany('App\Models\Variant');
    }

    /**
     * one to many relation between Model and Products
     * @return [type] [description]
     */
    public function products()
    {
      return $this->hasMany('App\Models\Product');
    }
}
