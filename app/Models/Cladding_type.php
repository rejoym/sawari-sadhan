<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cladding_type extends Model
{
    /**
     * arrays that are mass assigbable
     * @var array
     */
    protected $fillable = ['cladding_type'];

    /**
     * function to establish relation between cladding and cladding_type
     * @return [type] [description]
     */
    public function cladding()
    {
      return $this->hasMany('App\Models\Cladding');
    }
}
