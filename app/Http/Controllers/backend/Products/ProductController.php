<?php

namespace App\Http\Controllers\backend\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostProductModulesRequest;
use App\Models\Brand as Brand;
use App\Models\Modal as Modal;
use App\Models\Variant as Variant;
use App\Models\Product as Product;
use App\Models\Condition as Condition;
use App\Models\Wheel as Wheel;
use App\Models\Product_type as Type;
use App\Models\Product_release as Product_release;
use App\Models\Fuel as Fuel;
use App\Models\Transmission as Transmission;


class ProductController extends Controller
{

    /**
     * insert data into the brands
     * @param  array  $data [data to be inserted]
     * @return [array]       [description]
     */
    protected function createBrand(array $data)
    {
        return Brand::create($data);
    }

    /**
     * insert data into model
     * @param  array  $data [data to be inserted]
     * @return [array]       [description]
     */
    protected function createModel(array $data)
    {
        return Modal::create($data);
    }

    /**
     * insert data into variants
     * @param  array  $data [data to be inserted]
     * @return [array]       [description]
     */
    protected function createVariant(array $data)
    {
        return Variant::create($data);
    }

    /**
     * [createProduct description]
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    protected function createProduct(array $data)
    {
        return Product::create($data);
    }

    /**
     * function to return the list page of products
     * @return [view] [description]
     */
    public function getProduct()
    {
        $data['all_product'] = Product::with(['brand', 'modal', 'variant', 'fuel', 'transmission', 'condition', 'productReleaseDate', 'wheel', 'productType'])->get();
        return view('backend.product.product_list', compact('data'));
    }

    /**
     * [getCreateProduct description]
     * @return [type] [description]
     */
    public function getCreateProduct()
    {
        $data['all-wheel'] = Wheel::all();
        $data['all-type'] = Type::all();
        $data['all-condition'] = Condition::all();
        $data['all_brand'] = Brand::all();
        $data['all_release_date'] = Product_release::all();
        $data['all_fuel_type'] = Fuel::all();
        $data['all_transmission_type'] = Transmission::all();
        return view('backend.product.create_product', compact('data'));
    }
    /**
     * function to redirect to the brand list page
     * @return [view] [description]
     */
    public function getBrandList()
    {
        $data['all_brand'] = Brand::all();
        return view('backend.product.brand_list', compact('data'));
    }

    /**
     * function to redirect to the model list page
     * @return [view] [description]
     */
    public function getModelList()
    {
        $data['all_model_brand'] = Modal::with('brands')->get();
        $data['all_brand'] = Brand::all();
        return view('backend.product.model_list', compact('data'));
    }

    /**
     * function to redirect to the variant list page
     * @return [view] [description]
     */
    public function getVariantList()
    {
        $data['all_brand'] = Brand::all();
        $data['all_variant_relation'] = Variant::with('brands', 'modals')->get();
        return view('backend.product.variant_list', compact('data'));
    }

    /**
     * function to get brand with model for ajax request
     * @param  [type] $id [id of the brand]
     * @return [JSON]     [description]
     */
    public function findBrandWithModelId($id)
    {
        $model = Modal::where('brand_id', $id)->get();
        return response()->json($model);
    }

    /**
     * function to get variant with model for ajax request
     * @param  [integer] $id [model id]
     * @return [JSON]     [description]
     */
    public function findVariantWithModelId($id)
    {
        $variant = Variant::where('modal_id', $id)->get();
        return response()->json($variant);
    }

    /**
     * function to handle the post request of the brand and save it ot database
     * @param  PostProductModulesRequest $request [description]
     * @return [type]                             [description]
     */
    public function postBrand(PostProductModulesRequest $request)
    {
        $brand_name = $request->input('input-brand-name');
        $data_brand = [
            'brand_name' => $brand_name
        ];

        $insert_brand = $this->createBrand($data_brand);
        $request->session()->flash('Success', 'Brand Successfully Added');
        return redirect()->route('get.brand.list');
    }

    /**
     * function to handle post request of the model and save to the database
     * @param  PostProductModulesRequest $request [Form Request to check for validation]
     * @return [view]
     */
    public function postModel(PostProductModulesRequest $request)
    {
        $model_name = $request->input('input-model-name');
        $brand_id = $request->input('input-brand');
        $data_model = [
            'modal_name' => $model_name,
            'brand_id' => $brand_id
        ];

        $insert_model = $this->createModel($data_model);
        $request->session()->flash('Success', 'Model Successfully Added');
        return redirect()->route('get.model.list');
    }

    /**
     * [postVariant description]
     * @param  PostProductModulesRequest $request [description]
     * @return [type]                             [description]
     */
    public function postVariant(PostProductModulesRequest $request)
    {
        $variant_name = $request->input('input-variant-name');
        $brand_id = $request->input('input-brand');
        $model_id = $request->input('input-model');
        $data_variant = [
            'variant_name' => $variant_name,
            'brand_id' => $brand_id,
            'modal_id' => $model_id
        ];

        $insert_variant = $this->createVariant($data_variant);
        $request->session()->flash('Success', 'Variant Successfully Added');
        return redirect()->route('get.variant.list');
    }

    /**
     * function to insert product details into the database
     * @param  PostProductModulesRequest $request [validation for the forms]
     * @return [view] [view to be loaded after form submit]
     */
    public function postProduct(PostProductModulesRequest $request)
    {
        //generate a random string for product code
        $product_code = $this->randomString(3) .substr(time(), 0, 5);
        $brand_id = $request->input('input-brand');
        $model_id = $request->input('input-model');
        $variant_id = $request->input('input-variant');
        $price = $request->input('input-price');
        $wheel = $request->input('input-wheels');
        $condition = $request->input('input-condition');
        $product_type = $request->input('input-product-for');
        $release_date = $request->input('input-release-year');
        $fuel = $request->input('input-fuel-type');
        $transmission = $request->input('input-transmission');
        $milage = $request->input('input-milage');
        $data_product = [
            'product_code' => $product_code,
            'wheel_id' => $wheel,
            'condition_id' => $condition,
            'product_type_id' => $product_type,
            'brand_id' => $brand_id,
            'modal_id' => $model_id,
            'variant_id' => $variant_id,
            'product_release_id' => $release_date,
            'price' => $price,
            'fuel_id' => $fuel,
            'transmission_id' => $transmission,
            'milage' => $milage
        ];
        $insert_product = $this->createProduct($data_product);
        $request->session()->flash('Success', 'Product Successfully Added');
        return redirect()->route('get.product.list');
    }
    /**
     * function to generate a random string for product code
     * @param  [integer] $length [length to the string to be generated]
     * @return [character]         [the random string that was generated]
     */
    public function randomString($length)
    {
        //the characters used to generate a random string
        $permitted_characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        //generate a random string using the above specified characters
        $random = substr(str_shuffle($permitted_characters), 0, $length);
        return $random;
    }
}




