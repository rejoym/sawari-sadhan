<?php

namespace App\Http\Controllers\backend\ExteriorComponents;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Location as Location;
use App\Models\Antenna as Antenna;
use App\Models\Alloy_wheel_type as Alloy_wheel;
use App\Models\Radiator_grill as Radiator;
use App\Models\Doorhandle_finish as Doorhandle_finish;
use App\Models\Body_part as Body_part;
use App\Models\Body_part_color as Body_part_color;
use App\Models\Color as Color;
use App\Models\Lamp_type as Lamp_type;
use App\Models\Lamp as Lamp;
use App\Models\Garnish_type as Garnish_type;
use App\Models\Garnish as Garnish;
use App\Models\Molding_type as Molding_type;


class ExteriorComponentsController extends Controller
{
    /**
     * [getLocation description]
     * @return [type] [description]
     */
    public function getCreateLocation()
    {
      $data['all_location'] = Location::all();
      return view('backend.exterior_components.create_location', compact('data'));
    }

    /**
     * [postCreateLocation description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreateLocation(Request $request)
    {
      $location = $request->input('input-location');
      $insert_location = Location::create([
        'location' => $location
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.location.create');
    }

    /**
     * [getCreateAntenna description]
     * @return [type] [description]
     */
    public function getCreateAntenna()
    {
      $data['all_antenna'] = Antenna::all();
      return view('backend.exterior_components.create_antenna', compact('data'));
    }

    /**
     * [getAntenna description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreateAntenna(Request $request)
    {
      $antenna = $request->input('input-antenna');
      $insert_antenna = Antenna::create([
        'antenna' => $antenna
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.antenna.create');
    }

    /**
     * [getCreateAlloyWheel description]
     * @return [type] [description]
     */
    public function getCreateAlloyWheel()
    {
      $data['all_alloy_wheel'] = Alloy_wheel::all();
      return view('backend.exterior_components.create_alloy_wheel', compact('data'));
    }

    /**
     * [postCreateAlloyWheel description]
     * @return [type] [description]
     */
    public function postCreateAlloyWheel(Request $request)
    {
      $alloy_wheel = $request->input('input-alloy-wheel');
      $insert_alloy_wheel = Alloy_wheel::create([
        'alloy_wheel_type' => $alloy_wheel
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.alloywheel.create');
    }

    /**
     * [getRadiatorGrill description]
     * @return [type] [description]
     */
    public function getCreateRadiatorGrill()
    {
      $data['all_radiator_grill'] = Radiator::all();
      return view('backend.exterior_components.create_radiator', compact('data'));
    }

    /**
     * [postCreateRadiatorGrill description]
     * @return [type] [description]
     */
    public function postCreateRadiatorGrill(Request $request)
    {
      $radiator = $request->input('input-radiator-grill');
      $insert_radiator = Radiator::create([
        'radiator_grill' => $radiator
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.radiator.create');
    }

    /**
     * [getCreateDoorHandleFinish description]
     * @return [type] [description]
     */
    public function getCreateDoorHandleFinish()
    {
      $data['all_doorhandle'] = Doorhandle_finish::all();
      return view('backend.exterior_components.create_doorhandle', compact('data'));
    }

    /**
     * [postCreateDoorHandleFinish description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreateDoorHandleFinish(Request $request)
    {
      $finish = $request->input('input-doorhandle');
      $insert_doorhandle = Doorhandle_finish::create([
        'doorhandle_finish' => $finish
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.doorhandle.create');
    }


    /**
     * [getCreateBodyPart description]
     * @return [type] [description]
     */
    public function getCreateBodyPart()
    {
      $data['all_bodypart'] = Body_part::all();
      return view('backend.exterior_components.create_bodypart', compact('data'));
    }

    /**
     * [postCreateBodyPart description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreateBodyPart(Request $request)
    {
      $body_part = $request->input('input-body-part');
      $insert_body_part = Body_part::create([
        'body_part' => $body_part
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.bodypart.create');
    }

    /**
     * [getCreateBodyPartColor description]
     * @return [type] [description]
     */
    public function getCreateBodyPartColor()
    {
      $data['all_body_part'] = Body_part::all();
      $data['all_color'] = Color::all();
      $data['all_body_part_color'] = Body_part_color::with('color', 'bodyPart')->get();
      return view('backend.exterior_components.create_bodypartcolor', compact('data'));
    }

    /**
     * [postCreateBodyPartColor description]
     * @return [type] [description]
     */
    public function postCreateBodyPartColor(Request $request)
    {
      $body_part = $request->input('input-body-part');
      $color = $request->input('input-color');
      $insert_bodypart_color = Body_part_color::create([
        'body_part_id' => $body_part,
        'color_id' => $color
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.bodypartcolor.create');
    }

    /**
     * [getCreateLampType description]
     * @return [type] [description]
     */
    public function getCreateLampType()
    {
      $data['all_lamp_type'] = Lamp_type::all();
      return view('backend.exterior_components.create_lamptype', compact('data'));
    }

    /**
     * [postCreateLampType description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreateLampType(Request $request)
    {
      $lamp_type = $request->input('input-lamp-type');
      $insert_into_lamp_type = Lamp_type::create([
        'lamp_type' => $lamp_type
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.lamptype.create');
    }

    /**
     * [getCreateLamp description]
     * @return [type] [description]
     */
    public function getCreateLamp()
    {
      $data['all_lamp_type'] = Lamp_type::all();
      $data['all_location'] = Location::all();
      $data['all_lamp'] = Lamp::with('lampType', 'location')->get();
      return view('backend.exterior_components.create_lamp', compact('data'));
    }

    /**
     * [postCreateLamp description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreateLamp(Request $request)
    {
      $location = $request->input('input-location');
      $lamp = $request->input('input-lamp-type');
      $insert_into_lamp = Lamp::create([
        'location_id' => $location,
        'lamp_type_id' => $lamp
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.lamps.create');
    }

    /**
     * [getCreateGarnish description]
     * @return [type] [description]
     */
    public function getCreateGarnishType()
    {
      $data['all_garnish_type'] = Garnish_type::all();
      return view('backend.exterior_components.create_garnishtype', compact('data'));
    }

    /**
     * [postCreateGarnishType description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreateGarnishType(Request $request)
    {
      $garnish_type = $request->input('input-garnish-type');
      $insert_into_garnish_type = Garnish_type::create([
        'garnish_type' => $garnish_type
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.garnishtype.create');
    }

    /**
     * [getCreateGarnish description]
     * @return [type] [description]
     */
    public function getCreateGarnish()
    {
      $data['all_garnish_type'] = Garnish_type::all();
      $data['all_location'] = Location::all();
      $data['all_garnish'] = Garnish::with('garnishType', 'location')->get();
      return view('backend.exterior_components.create_garnish', compact('data'));
    }


    public function postCreateGarnish(Request $request)
    {
      $garnish_type = $request->input('input-garnish-type');
      $location = $request->input('input-location');
      $insert_into_garnish = Garnish::create([
        'garnish_type_id' => $garnish_type,
        'location_id' => $location
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.garnish.create');
    }


    public function getCreateMoldingType()
    {
      $data['all_molding_type'] = Molding_type::all();
      return view('backend.exterior_components.create_moldingtype', compact('data'));
    }

    /**
     * [postCreateGarnishType description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postCreateMoldingType(Request $request)
    {
      $molding_type = $request->input('input-molding-type');
      $insert_into_molding_type = Molding_type::create([
        'molding_type' => $molding_type
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.moldingtype.create');
    }

    /**
     * [getCreateGarnish description]
     * @return [type] [description]
     */
    public function getCreateMolding()
    {
      $data['all_molding_type'] = Molding_type::all();
      $data['all_location'] = Location::all();
      $data['all_molding'] = Molding::with('moldingType', 'location')->get();
      return view('backend.exterior_components.create_molding', compact('data'));
    }


    public function postCreateMolding(Request $request)
    {
      $molding_type = $request->input('input-molding-type');
      $location = $request->input('input-location');
      $insert_into_garnish = Garnish::create([
        'molding_type_id' => $garnish_type,
        'location_id' => $location
      ]);
      $request->session()->flash('Success');
      return redirect()->route('get.molding.create');
    }
}
