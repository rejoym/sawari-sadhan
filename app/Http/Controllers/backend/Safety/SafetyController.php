<?php

namespace App\Http\Controllers\backend\Safety;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Braking_system_type as Braking_type;
use App\Models\Parking_assist_mechanism as Parking_assist;
use App\Models\Start_assist_type as Start_assist;
use App\Models\Body_structure_type as Body_structure;
use App\Models\Product as Product;
use App\Models\Central_locking as Central_locking;
use App\Models\Audio_video_navigation as AVN;
use App\Models\Safety as Safety;

class SafetyController extends Controller
{
    /**
     * [getListIndex description]
     * @return [type] [description]
     */
    public function getListIndex()
    {
      $data['all_safety'] = Safety::with('product.brand', 'product.modal', 'product.variant', 'centralLocking', 'audioVideoNavigation')->get();
      return view('backend.safety.safety_list', compact('data'));
    }

    /**
     * [getCreateList description]
     * @return [type] [description]
     */
    public function getCreateIndex()
    {
      $data['all_products'] = Product::with(['brand', 'modal', 'variant'])->get();
      $data['all_braking_system'] = Braking_type::all();
      $data['all_parking_assist'] = Parking_assist::all();
      $data['all_start_assist'] = Start_assist::all();
      $data['all_body_structure'] = Body_structure::all();
      return view('backend.safety.create_safety', compact('data'));
    }


    public function postCreateIndex(Request $request)
    {
      //insert data into central locking table
      $insert_into_central = Central_locking::create([
                                'door' => $request->input('input-door'),
                                'tailgate' => $request->input('input-tailgate')
                             ]);
      //insert posted data into avn
      $insert_into_avn = AVN::create([
                                'avn' => $request->input('input-avn'),
                                'avn_rear_view_camera_display' => $request->input('input-rear-view-camera')
                             ]);

      $product = $request->input('input-safety-product');
      $elec_brake = $request->input('input-elec-brake-dist');
      $elec_stab = $request->input('input-elec-stability-ctrl');
      $vehicle_stab = $request->input('input-vehicle-stab-ctrl');
      $mirror = $request->input('input-day-night-mirror');
      $child_seat = $request->input('input-child-seat');
      $immobilizer = $request->input('input-immobilizer');
      $front_fog_lamps = $request->input('input-front-fog');
      $rear_fog_lamp = $request->input('input-rear-defogger');
      $automatic_headlamps = $request->input('input-automatic-headlamps');
      $smart_pedal = $request->input('input-smart-pedal');
      $clutch_lock = $request->input('input-clutch-lock');
      $seat_belt = $request->input('input-seatbelt');
      $headlight_escort = $request->input('input-headlight-func');
      $isofix = $request->input('input-isofix');
      $dual_horn = $request->input('input-dualhorn');
      //grab the id of the data inserted into cental locking table
      $central_id = $insert_into_central['id'];
      //grab the id of the data inserted into avn
      $avn_id = $insert_into_avn['id'];

      $insert_safety = [
        'product_id' => $product,
        'electronic_brakeforce_distribution' => $elec_brake,
        'electronic_stability_control' => $elec_stab,
        'vehicle_stability_management_control' => $vehicle_stab,
        'day_night_rear_view_mirror' => $mirror,
        'child_seat_anchor' => $child_seat,
        'immobilizer' => $immobilizer,
        'front_fog_lamps' => $front_fog_lamps,
        'rear_defogger_with_timer' => $rear_fog_lamp,
        'automatic_head_lamps' => $automatic_headlamps,
        'central_locking_id' => $central_id,
        'smart_pedal' => $smart_pedal,
        'clutch_lock' => $clutch_lock,
        'audio_video_navigation_id' => $avn_id,
        'headlight_escort_function' => $headlight_escort,
        'seat_belt_pretensioners' => $seat_belt,
        'isofix' => $isofix,
        'dual_horn' => $dual_horn
      ];

      $insert_into_safety = Safety::create($insert_safety);
      $request->session()->flash('Success', 'Safety specifications successfully added');
      return redirect()->route('get.safety.list');

    }
}
