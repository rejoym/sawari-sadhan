<?php

namespace App\Http\Controllers\backend\Exterior;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product as Product;
use App\Models\Alloy_wheel_type as Alloy_wheel;
use App\Models\Radiator_grill as Radiator_grill;
use App\Models\Doorhandle_finish as Doorhandle_finish;
use App\Models\Antenna as Antenna;

class ExteriorController extends Controller
{

    public function getCreateIndex()
    {
      $data['all_alloy_wheel'] = Alloy_wheel::all();
      $data['all_radiator_grill'] = Radiator_grill::all();
      $data['all_door_finish'] = Doorhandle_finish::all();
      $data['all_product'] = Product::all();
      $data['all_antenna'] = Antenna::all();
      return view('backend.exterior.create_exterior', compact('data'));
    }
}
