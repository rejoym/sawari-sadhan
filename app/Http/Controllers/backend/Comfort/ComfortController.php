<?php

namespace App\Http\Controllers\backend\Comfort;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product as Product;
use App\Models\Keyless_entry_type as Keyless_entry;
use App\Models\Air_conditioning_type as Air_condition;
use App\Models\Outside_mirror_type as Outside_mirror;
use App\Models\Power_windows_type as Power_window;
use App\Models\Washer_wiper_type as Washer_type;
use App\Models\Comfort as Comfort;
use App\Models\Comfort_keyless_entry_type as Comfort_keyless;
use App\Models\Comfort_air_conditioning_type as Comfort_aircondition;
use App\Models\Comfort_outside_mirror_type as Comfort_outside_mirror;
use App\Models\Comfort_power_windows_type as Comfort_power_window;
use App\Models\Comfort_washer_wiper_type as Comfort_washer_wiper;

class ComfortController extends Controller
{
    /**
     * [getKeylessIndex description]
     * @return [type] [description]
     */
    public function getKeylessIndex()
    {
      $data['all_keyless_entry'] = Keyless_entry::all();
      $data['all_product'] = Product::all();
      return view('backend.comfort.keyless_entry', compact('data'));
    }

    /**
     * [getAirConditionIndex description]
     * @return [type] [description]
     */
    public function getAirConditionIndex()
    {
      $data['all_air_condition'] = Air_condition::all();
      $data['all_product'] = Product::all();
      return view('backend.comfort.air_condition', compact('data'));
    }

    /**
     * [getMirrorIndex description]
     * @return [type] [description]
     */
    public function getMirrorIndex()
    {
      $data['all_mirror_type'] = Outside_mirror::all();
      $data['all_product'] = Product::all();
      return view('backend.comfort.mirror', compact('data'));
    }

    /**
     * [getPowerWindowIndex description]
     * @return [type] [description]
     */
    public function getPowerWindowIndex()
    {
      $data['all_power_window'] = Power_window::all();
      $data['all_product'] = Product::all();
      return view('backend.comfort.power_window', compact('data'));
    }

    /**
     * [getWasherWiperIndex description]
     * @return [type] [description]
     */
    public function getWasherWiperIndex()
    {
      $data['all_washer_type'] = Washer_type::all();
      $data['all_product'] = Product::all();
      return view('backend.comfort.washer_wiper_type', compact('data'));
    }

    /**
     * function to handle the post request of the keyless entry
     * @return [type] [description]
     */
    public function postKeylessEntry(Request $request)
    {
      //get the product id of the product selected
      $product_id = $request->input('input-product-keyless');
      //insert product id into the comfort table
      $insert_into_comfort = Comfort::create([
                                'product_id' => $product_id
                             ]);
      //get the id of the comfort table
      $comfort_id = $insert_into_comfort['id'];
      $keyless = $request->input('input-keyless-entry');
      foreach($keyless as $keys)
      {
        Comfort_keyless::create([
          'comfort_id' => $comfort_id,
          'keyless_entry_type_id' => $keys
        ]);
      }
      $request->session()->flash('Success', 'Keyless entry type successfully added');
      return redirect()->route('get.comfort.keyless');
    }

    /**
     * function to handle the post request of the air condition
     * @return [type] [description]
     */
    public function postAirCondition(Request $request)
    {
      $product_id = $request->input('input-product-aircondition');
      $insert_into_comfort = Comfort::create([
                                'product_id' => $product_id
                             ]);
      $comfort_id = $insert_into_comfort['id'];
      $aircondition = $request->input('input-aircondition');
      foreach($aircondition as $conditions)
      {
        Comfort_aircondition::create([
          'comfort_id' => $comfort_id,
          'air_conditioning_type_id' => $conditions
        ]);
      }
      $request->session()->flash('Success', 'Air condition type successfully added');
      return redirect()->route('get.comfort.aircondition');
    }

    /**
     * function to handle post request of outside mirror
     * @return [type] [description]
     */
    public function postOutsideMirror(Request $request)
    {
      $product_id = $request->input('input-comfort-product');
      $insert_into_comfort = Comfort::create([
                                'product_id' => $product_id
                             ]);
      $comfort_id = $insert_into_comfort['id'];
      $mirrors = $request->input('input-comfort-mirror');
      foreach($mirrors as $mirror)
      {
        Comfort_outside_mirror::create([
          'comfort_id' => $comfort_id,
          'outside_mirror_type_id' => $mirror
        ]);
      }
      $request->session()->flash('Success', 'Outside Mirror type successfully successfully added');
      return redirect()->route('get.comfort.mirror');
    }

    /**
     * function to handle the post request of the power window
     * @return [type] [description]
     */
    public function postPowerWindow(Request $request)
    {
      $product_id = $request->input('input-product-powerwindow');
      $insert_into_comfort = Comfort::create([
                                'product_id' => $product_id
                             ]);
      $comfort_id = $insert_into_comfort['id'];
      $windows = $request->input('input-comfort-window');
      foreach($windows as $window)
      {
        Comfort_power_window::create([
          'comfort_id' => $comfort_id,
          'power_windows_type_id' => $window
        ]);
      }
      $request->session()->flash('Success', 'Outside Mirror type successfully successfully added');
      return redirect()->route('get.comfort.powerwindow');
    }

    /**
     * function to handle the post request of the washer wiper
     * @return [type] [description]
     */
    public function postWasherWiper(Request $request)
    {
      $product_id = $request->input('input-product-washer');
      $insert_into_comfort = Comfort::create([
                                'product_id' => $product_id
                             ]);
      $comfort_id = $insert_into_comfort['id'];
      $washers = $request->input('input-comfort-washer');
      foreach($washers as $washer)
      {
        Comfort_washer_wiper::create([
          'comfort_id' => $comfort_id,
          'washer_wiper_type_id' => $washer
        ]);
      }
      $request->session()->flash('Success', 'Outside Mirror type successfully successfully added');
      return redirect()->route('get.comfort.washerwiper');
    }
}
?>
