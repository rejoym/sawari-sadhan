<?php

namespace App\Http\Controllers\frontend\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    /**
     * function to load the view page of the login
     * @return [view] [description]
     */
    public function getIndex()
    {
      return view('frontend._shared.login');
    }
}
