<?php

namespace App\Http\Controllers\frontend\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer_user as Customer_user;
use App\Models\VerifyUserByEmail as VerifyUser;
use App\Http\Requests\PostCustomerRegisterRequest;
use App\Exceptions\Handler;
use Mail;

class RegisterController extends Controller
{
    /**
     * view page of the register
     * @return [view] [description]
     */
    public function getIndex()
    {
        return view('frontend._shared.register');
    }

    /**
     * function to insert user details into the database
     * @param  PostCustomerRegisterRequest $request [validation request]
     * @return [route][route to describe the view that is to be loaded]
     */
    public function postRegister(PostCustomerRegisterRequest $request)
    {
      try
      {
        $fname = $request->input('input-fname');
        $mname = $request->input('input-mname');
        $lname = $request->input('input-lname');
        $uname = $request->input('input-username');
        $email = $request->input('input-email');
        $psw = $request->input('input-password');
        $gender = $request->input('input-gender');
        $city = $request->input('input-city');
        $dob = $request->input('input-dob');
        $phone = $request->input('input-phone');
        $country = $request->input('input-country');
        $reg_date = date("Y-m-d h:i:s a", time());
        $address = $request->input('input-address');
        $data_user = [
          'email' => $email,
          'password' => bcrypt($psw),
          'first_name' => $fname,
          'middle_name' => $mname,
          'last_name' => $lname,
          'username' => $uname,
          'gender' => $gender,
          'date_of_birth' => $dob,
          'city' => $city,
          'country' => $country,
          'registration_date' => $reg_date,
          'phone_number' => $phone,
          'address' => $address
        ];

        $register_user = $this->createUser($data_user);

        $check_send = false;

        $uid = $register_user['id'];
        $verifyUser = VerifyUser::create([
          'user_id' => $uid,
          'token' => str_random(40)
        ]);
        $check_send = $this->sendEmail();
        if($check_send === true) {
          $request->session()->flash('Success', 'New User Registered Successfully!! Please activate your account from the link sent in your email');
          return redirect()->route('get.customer.login');
        }
        // else {
        //   echo "error";
        // }
      }catch(\Exception $e) {
        // report($e);
        echo $e;
        // return redirect()->back();
      }
    }

    /**
     * function to send email
     * @return [boolean] [returns true if mail successfully sent else return false]
     */
    public function sendEmail()
    {
      try{
        $data = VerifyUser::with('users')->get();
        Mail::send('frontend.emails.verifyUsers', ['user' => $data], function($message) use($data){
          $message->to($data[0]['users']['email'], $data[0]['users']['username']);
          $message->subject("Activation Code");
        });
        return true;
      }
      catch(\Exception $e){
        // report($e);
        echo $e;
        // return false;
      }

    }

    /**
     * function to verify the user
     * @param  [string] $token [token of each individual user]
     * @return [route]        [description]
     */
    public function verifyUser($token)
    {
      $verify = VerifyUser::where('token', $token)->first();
      if($verify) {
        //variable to store all the users from customer_user table defined in VerifyUser table
        $user = $verify->users;
        if(!$user->email_verified) {
          $verify->users->email_verified = 1;
          $verify->users->save();
          $status = "Your email is verified. Account Activated";
        }
        else {
          $status = "Account Already Verfied !";
        }
      }
      else {
        return redirect()->route('get.customer.login')->with('warning', "Sorry! Your email could not be identified");
      }
      return redirect()->route('get.customer.login')->with('status', $status);
    }


    public function createUser(array $data)
    {
      return Customer_user::create($data);
    }

}
