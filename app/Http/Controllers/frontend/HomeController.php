<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * function to display the home page
     * @return [view] [description]
     */
    public function index()
    {
      return view('frontend.fourwheeler.home');
    }

    /**
     * [getUnderConstruction description]
     * @return [type] [description]
     */
    public function getUnderConstruction()
    {
      return view('frontend.welcome');
    }
}
