<?php

namespace App\Http\Controllers\Api\v1;

use DB;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Modal;
use App\Models\Variant;
use App\Models\Product;

class ApiController extends Controller
{
    //array variable to store the format of the JSON responses
    private $response = ['data' => '', 'status' => '', 'message' => ''];

    private function fetchResponse($data, $statusCode, $message)
    {
        $this->response['data'] = $data;
        $this->response['status'] = $statusCode;
        $this->response['message'] = $message;
        return Response::JSON($this->response);
    }
    /**
     * get all brands
     * @return objects [description]
     */
    public function getAllBrands()
    {
      return Brand::all();
    }

    /**
     * get brand by id
     * @param  [integer] $id [description]
     * @return [object]     [description]
     */
    public function getBrandById($id)
    {
      return Brand::find($id);
    }

    /**
     * get all models with brand
     * @return [object] [description]
     */
    public function getAllModels()
    {
        return Modal::with('brands')->get();
    }

    /**
     * get models by id
     * @param  [integer] $id [description]
     * @return [object]     [description]
     */
    public function getModelById($id)
    {
        return Modal::find($id);
    }

    /**
     * get all variants
     * @return [object] [description]
     */
    public function getAllVariants()
    {
        return Variant::all();
    }

    /**
     * get variant by id
     * @param  [integer] $id [description]
     * @return [object]     [description]
     */
    public function getVariantById($id)
    {
        return Variant::find($id);
    }

    /**
     * get all product
     * @return [object] [description]
     */
    public function getAllProducts()
    {
        try
        {
            $product = DB::table('products')
                      ->join('brands', 'products.brand_id', '=', 'brands.id')
                      ->join('modals', 'products.modal_id', '=', 'modals.id' )
                      ->join('variants', 'products.variant_id', '=', 'variants.id')
                      ->join('product_releases', 'products.product_release_id', '=', 'product_releases.id')
                      ->join('fuels', 'products.fuel_id', '=', 'fuels.id')
                      ->join('transmissions', 'products.transmission_id', '=', 'transmissions.id')
                      ->join('wheels', 'products.wheel_id', '=', 'wheels.id')
                      ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
                      ->join('conditions', 'products.condition_id', '=', 'conditions.id')
                      ->select('brands.brand_name', 'modals.modal_name', 'variants.variant_name', 'product_releases.product_release_date', 'fuels.fuel_type', 'transmissions.transmission_type', 'wheels.wheel_count', 'product_types.product_type', 'conditions.condition')
                      ->get();
            if(count($product)>0)
                return $this->fetchResponse($product, 200, 'New Product List');
            else
                return $this->fetchResponse($product, 204, 'No data exists');
        }
        catch(\Exception $e) {
            return $this->fetchResponse('', 400, 'Request could not be completed at this time');
        }

    }

    /**
     * get product by id
     * @param  [integer] $id [description]
     * @return [object]     [description]
     */
    public function getProductById($id)
    {
        return Product::find($id);
    }
}
