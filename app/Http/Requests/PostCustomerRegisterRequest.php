<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCustomerRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'input-fname' => 'required|max:255',
            'input-lname' => 'required|max:255',
            'input-username' => 'required|max:255|unique:customer_users,username',
            'input-email' => 'required|email|max:255|unique:customer_users,email',
            'input-password' => 'required|min:6',
            'input-gender' => 'required',
            'input-city' => 'required',
            'input-dob' => 'required',
            'input-phone' => 'required',
            'input-country' => 'required',
            'input-address' => 'required'
        ];
    }
}
