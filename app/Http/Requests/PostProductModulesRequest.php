<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class PostProductModulesRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(Request::is('admin/product/all-brand')) {
            return [
                'input-brand-name' => 'required'
            ];
        }
        else if(Request::is('admin/product/all-model')) {
            return [
                'input-model-name' => 'required',
                'input-brand' => 'required'
            ];
        }
        else if(Request::is('admin/product/all-variant')) {
            return [
                'input-variant-name' => 'required',
                'input-brand' => 'required',
                'input-model' => 'required'
            ];
        }
        else {
            return [
                'input-wheels' => 'required',
                'input-condition' => 'required',
                'input-product-for' => 'required',
                'input-release-year' => 'required',
                'input-brand' => 'required',
                'input-model' => 'required',
                'input-variant' => 'required',
                'input-price' => 'required',
                'input-milage' => 'required',
                'input-fuel-type' => 'required',
            ];
        }

    }



    public function messages()
    {
        return [
            'input-brand-name.required' => 'Brand Name is required',
            'input-model-name.required' => 'Model Name is required',
            'input-variant-name.required' => 'Variant Name is required',
            'input-brand.required' => 'Please Select a Brand',
            'input-model.required' => 'Please Select a Model',
            'input-price.required' => 'Price is required',
            'input-variant.required' => 'Please Select a Variant',
            'input-wheels.required' => 'Please select the number of wheels',
            'input-condition.required' => 'Please select condition',
            'input-product-for.required' => 'Please specify whether you want to sell or rent',
            'input-release-year.required' => 'Please select product release year',
            'input-fuel-type.required' => 'Please select a fuel type',
            'input-milage.required' => 'Please select a mileage'
        ];
    }
}
