<?php

namespace App\Helpers;

class Helper {

  public static function hideLoginButtons($button) {
    $last_url = last(request()->segments());
    if ($last_url == 'login' || ($last_url == 'signup' && $button == 'signup')) {
      return "none";
    }
  }

  public static function setActiveMenu($menu_item){
    $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
    if (in_array($menu_item, $segments)) {
      return "current";
    }
  }

}

?>