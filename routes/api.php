<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function() {
  Route::get('/product/all-brand', 'Api\v1\ApiController@getAllBrands');
  Route::get('/product/all-brand-by-id/{id}', 'Api\v1\ApiController@getBrandById');
  Route::get('/product/all-model', 'Api\v1\ApiController@getAllModels');
  Route::get('/product/all-model-by-id/{id}', 'Api\v1\ApiController@getModelById');
  Route::get('/product/all-variant', 'Api\v1\ApiController@getAllVariants');
  Route::get('/product/all-variant-by-id/{id}', 'Api\v1\ApiController@getVariantById');
  Route::get('/product/all-product', 'Api\v1\ApiController@getAllProducts');
  Route::get('/product/all-product-by-id/{id}', 'Api\v1\ApiController@getProductById');

});
