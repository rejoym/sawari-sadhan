<?php
// use \Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*=================================================================================================
==================================Routes for frontend==============================================
=================================================================================================*/

Route::get('/', ['uses' => 'frontend\HomeController@getUnderConstruction', 'as' => 'get.underconstruction' ]);
Route::get('/demo', ['uses' => 'frontend\HomeController@Index', 'as' => 'get.home' ]);
Route::get('customer/signup', ['uses' => 'frontend\Auth\RegisterController@getIndex', 'as' => 'get.signup']);
Route::get('/user/verify/{token}', 'frontend\Auth\RegisterController@verifyUser');
Route::post('customer/signup', ['uses' => 'frontend\Auth\RegisterController@postRegister', 'as' => 'post.signup']);
Route::get('customer/login', ['uses' => 'frontend\Auth\LoginController@getIndex', 'as' => 'get.customer.login']);



/*================================================================================================
==================================Routes for backend==============================================
=================================================================================================*/
Route::prefix('admin')->group( function() {
  Route::get('login', ['uses' => 'backend\Auth\LoginController@getAdminLogin', 'as' => 'login']);
  Route::post('login', ['uses' => 'backend\Auth\LoginController@postAdminLogin', 'as' => 'post.login']);
  Route::get('logout', ['uses' => 'backend\Auth\LoginController@getLogout', 'as' => 'get.logout']);


  Route::group(['middleware' => 'auth'], function() {
    /* ============================================Routes of product================================================================= */
    Route::get('dashboard', ['uses' => 'backend\DashboardController@index', 'as' => 'get.admin.dashboard']);
    Route::get('product/all-brand', ['uses' => 'backend\Products\ProductController@getBrandList', 'as' => 'get.brand.list']);
    Route::get('product/all-model', ['uses' => 'backend\Products\ProductController@getModelList', 'as' => 'get.model.list']);
    Route::get('product/all-variant', ['uses' => 'backend\Products\ProductController@getVariantList', 'as' => 'get.variant.list']);
    Route::get('product/all-product', ['uses' => 'backend\Products\ProductController@getProduct', 'as' => 'get.product.list']);
    Route::post('product/all-brand', ['uses' => 'backend\Products\ProductController@postBrand', 'as' => 'post.brand']);
    Route::post('product/all-model', ['uses' => 'backend\Products\ProductController@postModel', 'as' => 'post.model']);
    Route::post('product/all-variant', ['uses' => 'backend\Products\ProductController@postVariant', 'as' => 'post.variant']);
    Route::get('product/create', ['uses' => 'backend\Products\ProductController@getCreateProduct', 'as' => 'get.product.create']);
    Route::post('product/create', ['uses' => 'backend\Products\ProductController@postProduct', 'as' => 'post.create.product']);
    /* ========================================== End of product routes =============================================================== */


   /* =======================================Routes for ajax request============================================================================= */
    Route::post('product/all-variant/postAjax/{id}', ['uses' => 'backend\Products\ProductController@findBrandWithModelId']);
    Route::post('product/all-product-brand/postAjax/{id}', ['uses' => 'backend\Products\ProductController@findBrandWithModelId']);
    Route::post('product/all-product-variant/postAjax/{id}', ['uses' => 'backend\Products\ProductController@findVariantWithModelId']);
    /* =======================================End of routes for ajax request====================================================================== */


    /* =======================================Routes for  Safety================================================================================== */
    Route::get('product/safety', ['uses' => 'backend\Safety\SafetyController@getListIndex', 'as' => 'get.safety.list']);
    Route::get('product/safety/create', ['uses' => 'backend\Safety\SafetyController@getCreateIndex', 'as' => 'get.safety.create']);
    Route::post('product/safety/create', ['uses' => 'backend\Safety\SafetyController@postCreateIndex', 'as' => 'get.safety.create']);
    /* =======================================End of routes for safety============================================================================= */

    /* =======================================Routes for Comfort================================================================================== */
    Route::get('product/comfort/keyless', ['uses' => 'backend\Comfort\ComfortController@getKeylessIndex', 'as' => 'get.comfort.keyless']);
    Route::post('product/comfort/keyless', ['uses' => 'backend\Comfort\ComfortController@postKeylessEntry', 'as' => 'post.comfort.keyless']);
    Route::get('product/comfort/aircondition', ['uses' => 'backend\Comfort\ComfortController@getAirConditionIndex', 'as' => 'get.comfort.aircondition']);
    Route::post('product/comfort/aircondition', ['uses' => 'backend\Comfort\ComfortController@postAirCondition', 'as' => 'post.comfort.aircondition']);
    Route::get('product/comfort/mirror', ['uses' => 'backend\Comfort\ComfortController@getMirrorIndex', 'as' => 'get.comfort.mirror']);
    Route::post('product/comfort/mirror', ['uses' => 'backend\Comfort\ComfortController@postOutsideMirror', 'as' => 'post.comfort.mirror']);
    Route::get('product/comfort/powerwindow', ['uses' => 'backend\Comfort\ComfortController@getPowerWindowIndex', 'as' => 'get.comfort.powerwindow']);
    Route::post('product/comfort/powerwindow', ['uses' => 'backend\Comfort\ComfortController@postPowerWindow', 'as' => 'post.comfort.powerwindow']);
    Route::get('product/comfort/washerwiper', ['uses' => 'backend\Comfort\ComfortController@getWasherWiperIndex', 'as' => 'get.comfort.washerwiper']);
    Route::post('product/comfort/washerwiper', ['uses' => 'backend\Comfort\ComfortController@postWasherWiper', 'as' => 'post.comfort.washerwiper']);
    /* =======================================End of routes for safety============================================================================= */

    Route::get('product/exterior', ['uses' => 'backend\Exterior\ExteriorController@getListIndex', 'as' => 'get.exterior.list']);
    Route::get('product/exterior/create', ['uses' => 'backend\Exterior\ExteriorController@getCreateIndex', 'as' => 'get.exterior.create']);

    Route::get('components/location', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateLocation', 'as' => 'get.location.create']);
    Route::post('components/location', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateLocation', 'as' => 'post.location.create']);
    Route::get('components/antenna', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateAntenna', 'as' => 'get.antenna.create']);
    Route::post('components/antenna', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateAntenna', 'as' => 'post.antenna.create']);
    Route::get('components/alloy-wheel', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateAlloyWheel', 'as' => 'get.alloywheel.create']);
    Route::post('components/alloy-wheel', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateAlloyWheel', 'as' => 'post.alloywheel.create']);
    Route::get('components/radiator-grill', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateRadiatorGrill', 'as' => 'get.radiator.create']);
    Route::post('components/radiator-grill', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateRadiatorGrill', 'as' => 'post.radiator.create']);
    Route::get('components/doorhandle-finish', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateDoorHandleFinish', 'as' => 'get.doorhandle.create']);
    Route::post('components/doorhandle-finish', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateDoorHandleFinish', 'as' => 'post.doorhandle.create']);
    Route::get('components/bodypart', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateBodyPart', 'as' => 'get.bodypart.create']);
    Route::post('components/bodypart', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateBodyPart', 'as' => 'post.bodypart.create']);
    Route::get('components/bodypart-color', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateBodyPartColor', 'as' => 'get.bodypartcolor.create']);
    Route::post('components/bodypart-color', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateBodyPartColor', 'as' => 'post.bodypartcolor.create']);
    Route::get('components/lamp-type', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateLampType', 'as' => 'get.lamptype.create']);
    Route::post('components/lamp-type', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateLampType', 'as' => 'post.lamptype.create']);
    Route::get('components/lamp', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateLamp', 'as' => 'get.lamps.create']);
    Route::post('components/lamp', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateLamp', 'as' => 'post.lamps.create']);
    Route::get('components/garnish-type', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateGarnishType', 'as' => 'get.garnishtype.create']);
    Route::post('components/garnish-type', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateGarnishType', 'as' => 'post.garnishtype.create']);
    Route::get('components/garnish', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateGarnish', 'as' => 'get.garnish.create']);
    Route::post('components/garnish', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateGarnish', 'as' => 'post.garnish.create']);
    Route::get('components/molding-type', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@getCreateMoldingType', 'as' => 'get.moldingtype.create']);
    Route::post('components/molding-type', ['uses' => 'backend\ExteriorComponents\ExteriorComponentsController@postCreateMoldingType', 'as' => 'post.moldingtype.create']);
  });
});


?>
